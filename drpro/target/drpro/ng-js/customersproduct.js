app.controller('prodCtrl', function($scope,$http,$filter,$rootScope) {

	
	$scope.cid = window.location.hash;
	var customerid =$scope.cid.split("/");
	$scope.userid = customerid[2];
	
	$scope.productcatgory = function(){
		$scope.prodcategory;
	}
	
	$scope.saveproduct = function(){
		var data = {
			customer_productid:0,
			product_category:$scope.prodcategory,
			product_name:$scope.prodname,
			price:$scope.price,
			product_description:$scope.proddetails,
			customer_id:$scope.userid
		}
		
		updateimg(data);
	}
	
	$scope.saveuserproduct = function(data){
		$http.post("./rest/registration/saveuserproduct",data).then(function(response){
			$scope.saveproductdata = response.data;
			var someStr = $scope.saveproductdata;
			var accept = someStr.replace(/['"]+/g, '')
			
			if(accept == "ACCEPTED"){
				alert("Successfully Inserted");
				$scope.prodcategory="";
				$scope.prodname="";
				$scope.price="";
				$scope.proddetails="";
				$scope.userid="";
				$scope.prodimg="";
			}
		})
	}
	
	
	var updateimg = function(data){
		var files = angular.element(document.querySelector('.productimg'));
		if(files.val()!= 0){
			var formdata = new FormData();
		 	for(var i=0;i<files.length;i++){ 
		 		var fileObj= files[i].files;
		 		formdata.append("files" ,fileObj[0]);   
		    }
		 	
		 	 var xhr = new XMLHttpRequest();    
		 	 xhr.open("POST","./rest/file/upload/drproimg");
		 	xhr.send(formdata);
		    	xhr.onload = function(e) {
		    		if (this.status == 200) {
		    			//$scope.tmppath;
		    			var obj = JSON.parse(this.responseText);
		    			
		    			var imgpath = obj[0].path;
		    			
		    			data.product_image = "https://s3.amazonaws.com/magazinebucket/Uploadimg/"+imgpath;
		    			
		    			if(data.customer_productid == 0){
		    				$scope.saveuserproduct(data);
		    			}else{
		    				
		    			}
	    		    }
		    	};
		}else{
			$scope.saveuserproduct(data);
		}
	};
	
	$scope.viewproducts = function(){
		window.location.href="#/userproductview/"+$scope.userid;
	}
	
	var sortingOrder = 'name';
   	$scope.sortingOrder = sortingOrder;
    $scope.reverse = false;
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 12;
    $scope.pagedItems = [];
    $scope.currentPage = 0;
	
	$http.get("./rest/registration/productlist/"+$scope.userid).then(function(response){
		$scope.getproduct = response.data;
		
		var searchMatch = function (haystack, needle) {
	        if (!needle) {
	            return true;
	        }
	        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	    };
	    
	    $scope.search = function () {
	        $scope.filteredItems = $filter('filter')($scope.getproduct, function (item) {
	            for(var attr in item) {
	                if (searchMatch(item[attr], $scope.query))
	                    return true;
	            }
	            return false;
	        });
	        // take care of the sorting order
	        if ($scope.sortingOrder !== '') {
	            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	        }
	        $scope.currentPage = 0;
	        // now group by pages
	        $scope.groupToPages();
	    };
	    
	    
	    $scope.groupToPages = function () {
	        $scope.pagedItems = [];
	        
	        for (var i = 0; i < $scope.filteredItems.length; i++) {
	            if (i % $scope.itemsPerPage === 0) {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
	            } else {
	                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
	            }
	        }
	    };
		
	    
	    $scope.range = function (start, end) {
	        var ret = [];
	        if (!end) {
	            end = start;
	            start = 0;
	        }
	        for (var i = start; i < end; i++) {
	            ret.push(i);
	        }
	        return ret;
	    };
	    
	    $scope.prevPage = function () {
	        if ($scope.currentPage > 0) {
	            $scope.currentPage--;
	        }
	    };
	    
	    $scope.nextPage = function () {
	        if ($scope.currentPage < $scope.pagedItems.length - 1) {
	            $scope.currentPage++;
	        }
	    };
	    
	    $scope.setPage = function () {
	        $scope.currentPage = this.n;
	    };

	    // functions have been describe process the data for display
	    $scope.search();
	    
	});
	
	$scope.viewproductlist = true;
	$scope.showcartitem = function(){
		$scope.viewcartitem = true;
		$scope.viewproductlist = false;
	}
	
	$scope.showproductlist = function(){
		$scope.viewproductlist = true;
		$scope.viewcartitem = false;
	}
	
	$scope.total = 0;
	var itemCount = 0;
	$scope.cart = [];	
	$scope.addItemToCart = function(product){
		itemCount ++;
		$('.itemCount').html(itemCount).css('display', 'block');
		
		if($scope.cart.length == 0){
			product.count = 1;
			$scope.cart.push(product);
		}else{
			var repeat = false;
			for(var i=0;i<$scope.cart.length;i++){
				if($scope.cart[i].customer_productid==product.customer_productid){
					repeat = true;
					$scope.cart[i].count +=1;
				}
			}
			if(!repeat){
				product.count = 1;
				$scope.cart.push(product);
			}
		}
		var expireDate = new Date();
	    expireDate.setDate(expireDate.getDate() + 1);
			 
		$scope.total += parseFloat(product.price);
	  
	}
	
	$scope.removeItemCart = function(product){
		itemCount --;
		$('.itemCount').html(itemCount).css('display', 'block');
		if(product.count > 1){
		    product.count -= 1;
		    var expireDate = new Date();
		    expireDate.setDate(expireDate.getDate() + 1);
	   	}
		else if(product.count === 1){
			var index = $scope.cart.indexOf(product);
			$scope.cart.splice(index, 1);
			expireDate = new Date();
			expireDate.setDate(expireDate.getDate() + 1);
		}
		   
		    $scope.total -= parseFloat(product.price);
	}
	
});