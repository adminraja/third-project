app.controller('chartCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$compile) {

	$scope.cid = window.location.hash;
	var customerid =$scope.cid.split("/");
	var cid = customerid[2];
	   
	$http.get("./rest/registration/getanimalchart/"+cid).then(function(response){
		$scope.getanimalchart = response.data;
		petchart();
	});
	   
	$http.get("./rest/registration/getanimallist/"+cid).then(function(response){
		$scope.getanimallist = response.data;
		$scope.showpetlist = true;
		$scope.viewchart = function(data){
			var petid = data.animalid;
			var drange = data.disease_range;
			$scope.petname = data.animal_category;
			$scope.getrange = drange.split(',');
			$scope.range1 =$scope.getrange[0] +"%";
			$scope.range2 =$scope.getrange[1] +"%";
			$scope.range3 =$scope.getrange[2] +"%";
			$scope.range4 =$scope.getrange[3] +"%";
			$scope.showpetchart = true;
		}
	});	
	
	
	//Pie chart for animal
	
	function petchart(){
		var chartElement = d3.select("#chart svg");
		var chart;
	
		nv.addGraph(function() {
		  chart = nv.models.pieChart()
		    .x(function(d) {
			      var cate = d.label;
				  $("#acate").val(cate);
			      return d.label
		    })
		    .y(function(d) {
		      return d.value
		    })
		    .showLabels(true);
	
		  var chartData =  $scope.getanimalchart;
	
		  chartElement
		    .datum(chartData)
		    .call(chart);
	
		  chart.pie.dispatch.on("elementClick", function(e) {
		    $scope.animalcategory = e.data.label;
		    $scope.count = e.data.value;    
		  });
		  
		  $scope.animalcate = _.pluck($scope.getanimalchart,'label')
		  
		  return chart;
		});
	
	}
	
	
	$scope.getanimal = function(data){
		$http.get("./rest/registration/getanimalcategory/"+data+"/"+cid).then(function(response){
			$scope.getallanimaldata = response.data;
			$scope.getcattlelist = true;
		});
	}
	
	// Anychart modal
	
	/*function petchart(){
	   // set the data
	   var data = $scope.getanimalchart;
	   // create the chart
	   var chart = anychart.pie();
	   // set the chart title
	   chart.title("");
	   // add the data
	   chart.data(data);
	   // display the chart in the container
	   chart.container('container');
	   chart.draw();
	  
	}*/
	
	 	
/*	var my_array = fields;
	 
    for (var i=0; i<my_array.length; i++) {
        $scope.dd = (my_array[i]);
        //a b c
    }*/
    
    
	/*var my_json = $scope.getanimallist;

	var values = [];
	for(var i in my_json) {
	var data=my_json[i].disease_range;
		values.push(data);
	}*/
	   
});