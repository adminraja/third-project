
app.controller('userCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$compile,$location,$timeout) {
	
	   $scope.cid = window.location.hash;
	   var customerid =$scope.cid.split("/");
	   var cid = customerid[2];
	
	   $http.get("./rest/registration/getindividualuser/"+cid).then(function(response){
			$scope.getuser = response.data;
			$rootScope.userid = $scope.getuser[0].customer_id; 
			/*getusermap();*/
			$scope.customername = $scope.getuser[0].customer_name; 
	   });
	   
	  /* $http.get("./rest/registration/getindividualuser/"+cid).then(function(response){
			$scope.getuser = response.data;
	   });
	   */
	   
	   $rootScope.userid;
	  
	   $scope.petbtn = function(){
		 window.location.href="#/animal-register/"+$rootScope.userid;
	   }
	   
	   	var sortingOrder = 'name';
	   	$scope.sortingOrder = sortingOrder;
	    $scope.reverse = false;
	    $scope.filteredItems = [];
	    $scope.groupedItems = [];
	    $scope.itemsPerPage = 10;
	    $scope.pagedItems = [];
	    $scope.currentPage = 0;
				
	   //pet insert and get list
	   $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
			$scope.getanimallist = response.data;
			$scope.petcount = $scope.getanimallist.length;  
			getpetmap();
			
			if($scope.petcount == "23"){
				$scope.petcount = 0;
			}
			$scope.viewnotdata = false;
			if($scope.petcount == 0){
				$scope.viewpagi = false;
				$scope.viewnotdata = true;
			}else{
				$scope.viewpagi = true;
				$scope.viewnotdata = false;
			}
			
			$scope.getpetlists = $scope.getanimallist; 
			var someStr = $scope.getpetlists;
			//$scope.accept = someStr.replace(/['"]+/g, '');
					
			if(someStr == response.data){
				var searchMatch = function (haystack, needle) {
			        if (!needle) {
			            return true;
			        }
			        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
			    };
			    
			    $scope.search = function () {
			        $scope.filteredItems = $filter('filter')($scope.getpetlists, function (item) {
			            for(var attr in item) {
			                if (searchMatch(item[attr], $scope.query))
			                    return true;
			            }
			            return false;
			        });
			        // take care of the sorting order
			        if ($scope.sortingOrder !== '') {
			            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
			        }
			        $scope.currentPage = 0;
			        // now group by pages
			        $scope.groupToPages();
			    };
			    
			    
			    $scope.groupToPages = function () {
			        $scope.pagedItems = [];
			        
			        for (var i = 0; i < $scope.filteredItems.length; i++) {
			            if (i % $scope.itemsPerPage === 0) {
			                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
			            } else {
			                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
			            }
			        }
			    };
				
			    
			    $scope.range = function (start, end) {
			        var ret = [];
			        if (!end) {
			            end = start;
			            start = 0;
			        }
			        for (var i = start; i < end; i++) {
			            ret.push(i);
			        }
			        return ret;
			    };
			    
			    $scope.prevPage = function () {
			        if ($scope.currentPage > 0) {
			            $scope.currentPage--;
			        }
			    };
			    
			    $scope.nextPage = function () {
			        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			            $scope.currentPage++;
			        }
			    };
			    
			    $scope.setPage = function () {
			        $scope.currentPage = this.n;
			    };

			    // functions have been describe process the data for display
			    $scope.search();
			    /*$scope.accept = someStr.replace(/['"]+/g, '');
			    if($scope.accept =="INTERNAL_SERVER_ERROR"){
			    	var template = $('#hidden-template').html();
					$('#targetTable').append(template);
			    }*/
			}/*else{
				var template = $('#hidden-template').html();
				$('#targetTable').append(template);
				
			}	*/
			
				
			     
	   });
	   
	   $scope.petsearchlist = function() {
		   if($scope.searchpet == undefined){
			   window.location.href="#/dashboard/"+cid;
			   $http.get("./rest/registration/searchpetlist/"+$scope.searchpet+"/"+cid).then(function(response){
					  $scope.getsearchpetlist = response.data; 
				   });
			   $scope.getsearchpetlist;
		   }else{
			   $http.get("./rest/registration/searchpetlist/"+$scope.searchpet+"/"+cid).then(function(response){
				  $scope.getsearchpetlist = response.data; 
			   });
			   $scope.getsearchpetlist;	
		   }
		 }
	   
	   $http.get("./rest/doctordata/doctorpetchart").then(function(response){
			$scope.getpetcategory = response.data;
	   });
	   
	   //$("#address").geocomplete({details:"form#property"});
	   
	   
	   $scope.genderclk = function(){
		   $scope.petgender;
	   }
	   
	   $http.get("./rest/registration/getallpetlist").then(function(response){
		   $scope.getallpetlist = response.data;
	   });
	   
	   
	   $scope.statementFilter = '';
	   
	   /*$scope.uid = '';
		  if($scope.uid == ''){
			 $scope.duplicateid == false;
		  }*/
		  
		$scope.removeTagOnBackspace = function (event) {
		    if(event.keyCode == 8){
			  if($scope.uid != ''){
				  $scope.duplicateid = false;
			  }
		  }
		};
		  
	   $scope.savepet = function(){
		   		  
		  if($scope.petform.$valid){
			  
			  $scope.duplicateid = false;
			  angular.forEach($scope.getallpetlist, function(value, key){
				  if(value.uid == $scope.uid){
					  $scope.duplicateid = true;
					  if($scope.uid == ''){
						  $scope.duplicateid == false;
					  }
				  }
			  });
			  
			  if($scope.duplicateid == false){
				  if($scope.uid == ''){
					  $scope.duplicateid == false;
				  }
			  var geocoder = new google.maps.Geocoder();
			  var address = $scope.address;
			
			  geocoder.geocode( { 'address': address}, function(results, status) {
				
		         	 if (status == google.maps.GeocoderStatus.OK) {
						$scope.latitude = results[0].geometry.location.lat();
						$scope.longitude = results[0].geometry.location.lng();
			         }
				  
				     var data ={
				    		 animal_category:$scope.getpetlist,
							 address:$scope.address,
							 animal_name:$scope.petname,
							 latitude:$scope.latitude,
							 longitude:$scope.longitude,
							 age:$scope.age,
							 gender:$scope.petgender,
							 uid:$scope.uid,
							 customer_id:$rootScope.userid
					 }
				     
				     $http.post("./rest/registration/petregister",data).then(function(response){
					  	$scope.savepet = response.data; 
					  	//alert("Inserted Successfully");
					  	var someStr = $scope.savepet; 
						$scope.accept = someStr.replace(/['"]+/g, '');
						if($scope.accept == "ACCEPTED"){
							$scope.petsuccessmsg = true;
							 $timeout( function(){
								 $scope.petsuccessmsg = false;
								 window.location.reload();
								// clrdata();
						     }, 4000 );
						}
						
				   });
			     
			 });
			  }
			  
		  }
	   }
	   
	   	   
	   function clrdata(){
		   $scope.getpetlist="";
		   $scope.petname="";
		   $scope.latitude="";
		   $scope.longitude="";
		   $scope.age="";
		   $scope.gender="";
		   $scope.address="";
		   $scope.uid="";
	   }
	   
	   
	   $scope.hoverIn = function(){
	        this.hoverEdit = true;
	    };

	    $scope.hoverOut = function(){
	        this.hoverEdit = false;
	    };
	   

	   //for customer map view
	   var getpetlocation = $scope.getanimallist;
	   var addresses= _.pluck(getpetlocation,'address');
	   function getmap(addresses){
		   var geocoder = new google.maps.Geocoder();
		   //var address = $scope.petlocation; 
		   var getpetlocation = $scope.getanimallist;
		   var addresses= _.pluck(getpetlocation,'address');
		   var coords = [];
		   for(var i = 0; i < addresses.length; i++){
			   currAddress = addresses[i];
		        var geocoder = new google.maps.Geocoder();
		        if (geocoder) {
		            geocoder.geocode({'address':currAddress}, function (results, status) {
		                if (status == google.maps.GeocoderStatus.OK) {
		                	coords.push(results[0].geometry.location);
		                	if(coords.length == addresses.length) {
		                        if( typeof callback == 'function' ) {
		                            callback();
		                        }
		                    }
		                }
		                else {
		                    throw('No results found: ' + status);
		                }
		            });
		        }
		   }
		   
	   }
	   
	   getmap(addresses, function(){
		   
	   });
	   
	   
	   
	   function getpetmap(){
			angular.forEach($scope.getanimallist, function(value, key){
				
				 $scope.petlocation = value.address
				
				 var geocoder = new google.maps.Geocoder();
				 var address = $scope.petlocation; 
				 var getpetlocation = $scope.getanimallist;
				 var results= _.pluck(getpetlocation,'address');
				   
				 geocoder.geocode( { 'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						for(var i =0;i<results.length;i++){
							var latitude = results[i].geometry.location.lat();
							var longitude = results[i].geometry.location.lng();
										
					     $scope.MapOptions = {
					         center: new google.maps.LatLng(latitude, longitude),
					         zoom: 4,
					         mapTypeId: google.maps.MapTypeId.ROADMAP
					     };
						}
				    } 
			     var icon = { 
			  		    url: './images/cow.png'                             
			  	 };
			     
			     //Initializing the InfoWindow, Map and LatLngBounds objects.
			     $scope.InfoWindow = new google.maps.InfoWindow();
			     $scope.Latlngbounds = new google.maps.LatLngBounds();
			     $scope.Map = new google.maps.Map(document.getElementById("userMap"), $scope.MapOptions);
			
			     //Looping through the Array and adding Markers.
			     for (var i = 0; i < $scope.getanimallist.length; i++) {
			    	 var data = $scope.getanimallist[i];
			         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
			
			         //Initializing the Marker object.
			         var marker = new google.maps.Marker({
			             position: myLatlng,
			             map: $scope.Map,
			             animal_category: data.animal_category,
			             icon:icon
			         });
			
			         //Adding InfoWindow to the Marker.
			         (function (marker, data) {
			             google.maps.event.addListener(marker, "click", function (e) {
			                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
			                 $scope.InfoWindow.open($scope.Map, marker);
			             });
			         })(marker, data);
			
			         //Plotting the Marker on the Map.
			         $scope.Latlngbounds.extend(marker.position);
			     }
			
			     //Adjusting the Map for best display.
			     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
			     $scope.Map.fitBounds($scope.Latlngbounds);
			     
				 });
			 });
		 }
	   
	   
	   /*function getusermap(){
		   
		   $scope.useraddress = $scope.getuser[0].customer_street;
		   
		   var geocoder = new google.maps.Geocoder();
		   var address = $scope.useraddress; 
		   
		   geocoder.geocode( { 'address': address}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
				    } 
		  
		     $scope.MapOptions = {
		         center: new google.maps.LatLng(latitude, longitude),
		         zoom: 4,
		         mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		     
		     var icon = { 
		  		    url: './images/docicon.png'                             
		  	 };
		     
		     //Initializing the InfoWindow, Map and LatLngBounds objects.
		     $scope.InfoWindow = new google.maps.InfoWindow();
		     $scope.Latlngbounds = new google.maps.LatLngBounds();
		     $scope.Map = new google.maps.Map(document.getElementById("userMap"), $scope.MapOptions);
		
		     //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getuser.length; i++) {
		         var data = $scope.getuser[0];
		         var myLatlng = new google.maps.LatLng(latitude, longitude);
		
		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             customer_name: data.customer_name,
		             icon:icon
		         });
		
		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.customer_name + ',' + data.customer_street +  "</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);
		
		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }
		
		     //Adjusting the Map for best display.
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
		     
		   });		   
		 }*/
		 
	   
	   /*$('#i_file').change( function(event) {
			$scope.tmppath = URL.createObjectURL(event.target.files[0]);
		    $("#viewimg").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
			    
		    $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+$scope.tmppath+"]</strong>");
	   
		    $.ajax({
				url : 'GetUserServlet',
				data : {
					tmppath : $scope.tmppath
				},
				success : function(responseText) {
					console.log("responseText" ,+ responseText );
				}
			});
	
		});*/
	   
	   function readURL(input) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();

               reader.onload = function (e) {
                   $('#blah')
                       .attr('src', e.target.result);
               };

               reader.readAsDataURL(input.files[0]);
           }
       }
	   $("#profile-img").change(function(){
	        readURL(this);
	    });
	   
	   $scope.edituser = function(data){
		   $scope.customername=data.customer_name;
		   $scope.contactno=data.customer_phone;
		   $scope.address=data.customer_street;
		   $scope.userphoto = data.userphoto;
		   $scope.customer_id = data.customer_id;
	   }
       
	   $scope.updateview = false;
	   $scope.updateuserreg = function(){ 
		   if($scope.updateform.$valid){
			  var data = {
			    customer_name:$scope.customername,
				customer_street:$scope.address,
				customer_phone:$scope.contactno,
				userphoto:$scope.userphoto,
				customer_id:$scope.customer_id
			  }
		    updateimg(data);
		   }
       }
	   
	   $scope.updateuser = function(data){
		   $http.post("./rest/registration/userupdate",data).then(function(response){
				$scope.userupdate = response.data;
				var someStr = $scope.userupdate;
				var accept = someStr.replace(/['"]+/g, '')
				
				if(accept == "ACCEPTED"){
					 $scope.updateview = true;
					 $timeout( function(){
						 window.location.reload();
				     }, 3000 );
				}
				//alert("Updated Successfully");
				//window.location.reload();
			}); 
	   }
	   
	   var updateimg = function(data){
			var files = angular.element(document.querySelector('.imgupload'));
			if(files.val()!= 0){
				var formdata = new FormData();
			 	for(var i=0;i<files.length;i++){ 
			 		var fileObj= files[i].files;
			 		formdata.append("files" ,fileObj[0]);   
			    }
			 	
			 	 var xhr = new XMLHttpRequest();    
			 	 xhr.open("POST","./rest/file/upload/drproimg");
			 	xhr.send(formdata);
			    	xhr.onload = function(e) {
			    		if (this.status == 200) {
			    			//$scope.tmppath;
			    			var obj = JSON.parse(this.responseText);
			    			
			    			var imgpath = obj[0].path;
			    			
			    			data.userphoto = imgpath;
			    			
			    			if(data.customer_id == 0){
			    				$scope.senduserimg(data);
			    			}else{
			    				$scope.updateuser(data);
			    			}
		    		    }
			    	};
			}else{
				$scope.updateuser(data);
			}
		};
		
		
		
		// For animal graph 
		
		 /*$http.get("./rest/registration/getdiseasechart/"+cid).then(function(response){
				$scope.getdiseasechart = response.data;
				
				var chart = new CanvasJS.Chart("chartContainer", {
						theme:"light2",
						animationEnabled: true,
						title:{
							text: "Animal Disease Status"
						},
						axisY :{
							includeZero: false,
							title: "Disease Status",
							suffix: ""
						},
						toolTip: {
							shared: "true"
						},
						legend:{
							cursor:"pointer",
							//itemclick : toggleDataSeries
						},
						data: [{
							type: "spline",
							visible: true,
							showInLegend: true,
							yValueFormatString: "##.00",
							name: "pet",
							dataPoints: $scope.getdiseasechart
						}]
					  
					});
					chart.render();
					
					function toggleDataSeries(e) {
						if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
							e.dataSeries.visible = false;
						} else {
							e.dataSeries.visible = true;
						}
						chart.render();
					}
		 });*/
		
		 
		 $scope.getClass = function (path) {
			  return ($location.path().substr(0, path.length) === path) ? 'active' : '';
			}
		 
		 /*$("#menu li a").on('click', function(e) {
			 //e.preventDefault();
			 $("#menu li").hover(function() {
				    $(this).removeClass('grey').siblings().addClass('grey');
				}, function() {
				    $(this).addClass('grey').siblings('.active').removeClass('grey');
				    //
				}).on('click', function() {
				    $(this).removeClass('grey').addClass('active').siblings().addClass('grey').removeClass('active')
				});			 
		 });*/

	 
		 
		 //get pet entire history
		 $scope.getpethistory = function(petid){
			 $http.get("./rest/registration/getpethistory/"+petid).then(function(response){
				 $scope.pethistory = response.data; 
				/* $scope.cname = $scope.pethistory[0].customer_name;*/
			 });
		 }
		
		 
		//TO customer notification from doctor  
		$http.get("./rest/doctordata/confirmedusernotification/"+cid+"/"+"Confirmed"+"/"+"Not Shown").then(function(response){
			$scope.confirmednotification = response.data;
			$scope.notifycount = $scope.confirmednotification.length; 
		});
		
		$scope.viewconfirmupdate = function(datas){
			
			$scope.appointid = datas.appointmentid;
			$scope.confirmupdate = "shown";
			var data = {
					appointmentid:$scope.appointid,
					customerstatus:$scope.confirmupdate
			}
			
			$http.post("./rest/doctordata/confirmationupdate",data).then(function(response){
				$scope.confirmationupdate = response.data;
			});
			
		}
		
		
		$http.get("./rest/doctordata/shownnotification/"+cid+"/"+"shown").then(function(response){
			$scope.shownnotification = response.data;
			$scope.approvedcount = $scope.shownnotification.length; 
		});
		
		
		$scope.appointclk = function(data){
			$scope.petview = true; 
			$scope.docname = data.doctor_name;
			$scope.docemail = data.doctor_email;
			$scope.request = data.request;
			$scope.docphno = data.doctor_phoneno;
			$scope.acategory = data.animal_category;
			$scope.aname = data.animal_name;
			$scope.docdata = data.docappointmentdate;
		}
					 
		
		//Set Boundary
		$scope.setboundary = function(){
			window.location.href="#/petboundarymap/"+cid;
		}
		
		
		
		
		 function createChart() {
			 
			 //For getting category wise pet details
			 $scope.getlinechart = function(category	){
				 $http.get("./rest/registration/getpetcategorychart/"+cid+"/"+category).then(function(response){
						$scope.getpetcategorylist = response.data;
						
						var array = [];
						var arraypetname = [];
						var petnamelist = [];
							for (var i = 0; i < $scope.getpetcategorylist.length; i++) {
								array.push($scope.getpetcategorylist[i].name);
								arraypetname.push($scope.getpetcategorylist[i].name);
								petnamelist.push($scope.getpetcategorylist[i].animal_name);
							}
							
							$scope.petnamelst = petnamelist;
							function removeDuplicates(arr){
							    var unique_array = []
							    for(var i = 0;i < arr.length; i++){
							        if(unique_array.indexOf(arr[i]) == -1){
							            unique_array.push(arr[i])
							        }
							    }
							    return unique_array
							}

							array = removeDuplicates(array);
							//arraypetname = removeDuplicates(array);
							
							function getDiseaseArray(name){
								var array = [];
								var arraypetname = [];
								for (var i = 0; i < $scope.getpetcategorylist.length; i++) {
									if ($scope.getpetcategorylist[i].name == name)
									array.push($scope.getpetcategorylist[i].data);
									arraypetname.push($scope.getpetcategorylist[i].disease_name);
								}
								return array;
								return arraypetname;
							}
							
														
							//$scope.petnamefilter = array;

							var array1 = [];
							for (var j = 0; j < array.length; j++) {
								for (var i = 0; i < $scope.getpetcategorylist.length; i++) {
							   	   var object = {};
									object.name = array[j];
									object.data = getDiseaseArray(array[j]);
									object.disease_name = $scope.getpetcategorylist[i].disease_name;
									object.disease_date = $scope.getpetcategorylist[i].disease_date;
									array1.push(object);
									
									$scope.chartdata = array1;
								}
							 }
						
						
						
						$("#linechart").kendoChart({
			                title: {
			                    text: "Pet Disease Chart"
			                },
			                legend: {
			                    position: "bottom"
			                },
			                
			                seriesDefaults: {
			                    type: "line",
			                    style: "smooth"
			                },
			                series: $scope.chartdata,
			                valueAxis: {
			                    labels: {
			                        format: "{0}%"
			                    },
			                    line: {
			                        visible: true
			                    },
			                    axisCrossingValue: -10
			                },
			                categoryAxis: {
			                    categories: $scope.petnamelst,
			                    majorGridLines: {
			                        visible: false
			                    },
			                    labels: {
			                        rotation: "auto"
			                    }
			                },
			                tooltip: {
			                    visible: true,
			                    format: "{0}%",
			                    template: "<div>Disease Range: #= value #,</br> Disease Name: #= series.disease_name #,</br> Disease Date: #= series.disease_date#</div>"
			                }
			            });
			          						
				 });
					
			 }
			  
			 
			 //For getting all data
			 $http.get("./rest/registration/getdiseasechart/"+cid).then(function(response){
				$scope.getdiseasechart = response.data;
				
				//$scope.getdiseasechart = [{name:"dog", data:"20"},{name:"cat", data:"30"},{name:"dog", data:"40"},{name:"cat", data:"50"}];

				var array = [];
				var petnamelist = [];
					for (var i = 0; i < $scope.getdiseasechart.length; i++) {
						array.push($scope.getdiseasechart[i].name);
						petnamelist.push($scope.getdiseasechart[i].animal_name);
					}
					
					$scope.petnamelst = petnamelist;
					function removeDuplicates(arr){
					    var unique_array = []
					    for(var i = 0;i < arr.length; i++){
					        if(unique_array.indexOf(arr[i]) == -1){
					            unique_array.push(arr[i])
					        }
					    }
					    return unique_array
					}

					array = removeDuplicates(array);

					function getDiseaseArray(name){
						var array = [];
						for (var i = 0; i < $scope.getdiseasechart.length; i++) {
							if ($scope.getdiseasechart[i].name == name) 
								array.push($scope.getdiseasechart[i].data);
						}
						return array;
					}
					
					$scope.petfilter = array;

					var array1 = [];
					for (var j = 0; j < array.length; j++) {
							var object = {};
							object.name = array[j];
							object.data = getDiseaseArray(array[j]);
							//object.disease_name = $scope.getdiseasechart[i].disease_name;
							array1.push(object);
							
							$scope.chartdata = array1;
					}
				
				
				
				$("#linechart").kendoChart({
	                title: {
	                    text: "Pet Disease Chart"
	                },
	                legend: {
	                    position: "bottom"
	                },
	                
	                seriesDefaults: {
	                    type: "line",
	                    style: "smooth"
	                },
	                series: $scope.chartdata,
	                valueAxis: {
	                    labels: {
	                        format: "{0}%"
	                    },
	                    line: {
	                        visible: true
	                    },
	                    axisCrossingValue: -10
	                },
	                categoryAxis: {
	                    //categories: $scope.petnamelst,
	                    majorGridLines: {
	                        visible: false
	                    },
	                    labels: {
	                        rotation: "auto"
	                    }
	                },
	                tooltip: {
	                    visible: true,
	                    format: "{0}%",
	                    template: "Pet Category: #= series.name #, Disease Range: #= value #"
	                }
	            });
	            
			 });	
	        }

	        $(document).ready(createChart);
	        $(document).bind("kendo:skinChange", createChart);
		
      
	        
	       //For menu highlight
	       /* $( '#myDIV .navit-nav a' ).click(function () {
		    	$( '#myDIV .navit-nav' ).find( 'li.active' ).removeClass( 'active' );
		    	var URI = $( this ).parent( 'li' ).addClass( 'active' );
		    	window.location.path=URI.context.baseURI;
		    	$(this).dblclick(); 
		    });

	        
	        $( '#myDIV .navit-nav a' ).dblclick(function () {
	        	var URI = $( this ).parent( 'li' ).addClass( 'active' );
		    	window.location.path=URI.context.baseURI;
	        });*/
	    	
		 
	}).directive('myCustomer', function() {
    return {
        templateUrl: './ng-view/common/customer-header.html'
      };
    });




/*//Split category wise for line chart
var array=[];
var acate = _.pluck($scope.getdiseasechart,'label');
var auniq = _.uniq(acate);
var aname = _.pluck($scope.getdiseasechart,'animal_name');
_.each(auniq,function(data){
     var obj={};
     var array1 = [];
     _.each(aname,function(data1){
         var findname = _.findWhere($scope.getdiseasechart,{animal_name:data1});
         var petcate = findname.label;
         if(petcate == data){
             obj.animalid = findname.animalid;
             obj.animal_name = findname.animal_name;
             obj1 = {};
             obj1.animal_name = findname.animal_name;
             array1.push(obj1);
         }
     })
     obj.animal_name = array1;
     array.push(obj);
     
    $scope.petarray = array;
		
	$scope.acategory = aname;
		
});*/



/*var array1=[];
				angular.forEach($scope.getdiseasechart, function(value, key){
				     var object={};
				     var array2=[];
				     object.name = value.name;
				      angular.forEach($scope.getdiseasechart, function(value, key){
				          array2.push(value.data);
				      });
				      object.animal_name=array2;
				      array1.push(array2);
				});
				
				$scope.getval = [];
				[]
				for(var i=0;i<$scope.getdiseasechart.length;i++){
				     $scope.getval.push($scope.getdiseasechart[i].data)
				}*/