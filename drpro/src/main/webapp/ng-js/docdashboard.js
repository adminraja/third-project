app.controller('docCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$timeout) {
	
	$scope.cid = window.location.hash;
	var customerid =$scope.cid.split("/");
	$scope.doctid = customerid[2];
	
	$rootScope.docid = $scope.doctid; 
	
	$scope.docreg = function(){
		$scope.typeofreg = "vetdoctor";
	}
	
	$scope.loadview = false;
	$scope.savedocreg = function(){
		     
	  if($scope.vetdocform.$valid){
		
		  var geocoder = new google.maps.Geocoder();
		  var address = $scope.address;
		
		  geocoder.geocode( { 'address': address}, function(results, status) {
		
         	if (status == google.maps.GeocoderStatus.OK) {
         		$scope.latitude = results[0].geometry.location.lat();
				$scope.longitude = results[0].geometry.location.lng();
	        }
         	 
			var data = {
				doctor_name:$scope.docname,     
				doctor_email:$scope.docemail,
				doctor_phoneno:$scope.docphoneno,
				password:$scope.pwd,
				street:$scope.address,
				typeofreg:$scope.typeofreg,   
				latitude:$scope.latitude,
				longitude:$scope.longitude,
				city:$scope.city
			}
			$scope.loadview = true;
			$http.post("./rest/registration/vetdocregister",data).then(function(response){
				$scope.savedocdata = response.data;
				var someStr = $scope.savedocdata;
				var accept = someStr.replace(/['"]+/g, '')
				document.getElementById('load').style.visibility="visible";
				if(accept == "ACCEPTED"){
					$scope.loadview = false;
					$scope.successmsg = true;
					 $timeout( function(){
						 $scope.successmsg = false;
						 window.location.href="#/index-login";
				     }, 4000 );
				//	clrdata(); 
				}
			});
		 });
	  }
	}
	
	function clrdata(){
		$scope.docname="";     
		$scope.docemail="";
		$scope.docphoneno="";
		$scope.pwd="";
		$scope.location="";
	}
	
	  //doctor edit profile
	  $scope.editdoctor = function(data){
		   $scope.docname=data.doctor_name;
		   $scope.docphoneno=data.doctor_phoneno;
		   $scope.location=data.location;
		   $scope.getdocdate =data.dateandtime;
		   $scope.docstatus = data.doctor_status;
		   /*$scope.userphoto = data.userphoto;*/
		   $scope.doctorid = data.doctorid;
	  }
	  
	 
	  
	  $scope.updatedocreg = function(){ 
		  
		  var getdate = $scope.getdocdate.split(" ");
		  $scope.splitdate = getdate[1]+" "+getdate[2]+"-"+getdate[5]+"-"+getdate[6];
		  		  
		  var data = {
		    doctor_name:$scope.docname,
			location:$scope.location,
			doctor_phoneno:$scope.docphoneno,
			dateandtime:$scope.splitdate,
			doctor_status:$scope.docstatus,
			/*userphoto:$scope.userphoto,*/
			doctorid:$scope.doctorid
		  }
		  
		  $http.post("./rest/doctordata/doctorupdate",data).then(function(response){
				$scope.userupdate = response.data;
				alert("Updated Successfully");
				window.location.reload();
			});
	    
       }
	   
	   	
	
	// Get doctor id
	$http.get("./rest/doctordata/getdocid/"+$scope.doctid).then(function(response){
		$scope.getdocdata = response.data;
		docgetlocation();
    });
	
	function docgetlocation(){
		$scope.address = $scope.getdocdata[0].location; 
		
		var geocoder = new google.maps.Geocoder();
		var address = $scope.address;
	
		geocoder.geocode( { 'address': address}, function(results, status) {
	
		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
		  /*  alert(latitude);*/
		    } 
		 
	
	     $scope.MapOptions = {
	         center: new google.maps.LatLng(9.925201, 78.119774),
	         zoom: 15,
	         mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	     
	     var icon = { 
	  		    url: './images/doctor.png'                             
	  	 };
	     
	     //Initializing the InfoWindow, Map and LatLngBounds objects.
	     $scope.InfoWindow = new google.maps.InfoWindow();
	     $scope.Latlngbounds = new google.maps.LatLngBounds();
	     $scope.Map = new google.maps.Map(document.getElementById("docMap"), $scope.MapOptions);
	
	     //Looping through the Array and adding Markers.
	     for (var i = 0; i < $scope.getdocdata.length; i++) {
	         var data = $scope.getdocdata[0];
	         var myLatlng = new google.maps.LatLng(latitude, longitude);
	
	         //Initializing the Marker object.
	         var marker = new google.maps.Marker({
	             position: myLatlng,
	             map: $scope.Map,
	             doctor_name: data.customer_name,
	             icon:icon
	         });
	
	         //Adding InfoWindow to the Marker.
	         (function (marker, data) {
	             google.maps.event.addListener(marker, "click", function (e) {
	                 $scope.InfoWindow.setContent("<div>" + data.doctor_name + ',' + data.location + "</div>");
	                 $scope.InfoWindow.open($scope.Map, marker);
	             });
	         })(marker, data);
	
	         //Plotting the Marker on the Map.
	         $scope.Latlngbounds.extend(marker.position);
	     }
	
	     //Adjusting the Map for best display.
	     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
	     $scope.Map.fitBounds($scope.Latlngbounds);
		});
	}
   
	
	//for customerdetails.html
	$http.get("./rest/doctordata/customerview").then(function(response){
		$scope.getcustomerview = response.data;
    });
	
	$scope.getlocation = function(data){
		
		$scope.appointmentdata;
		
		var geocoder = new google.maps.Geocoder();
		var address = data.address;
	
		geocoder.geocode( { 'address': address}, function(results, status) {
	
		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
		} 
		 
	
	     $scope.MapOptions = {
	         center: new google.maps.LatLng(latitude, longitude),
	         zoom: 15,
	         mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	     
	     var icon = { 
	  		    url: './images/docicon.png'                             
	  	 };
	     
	     //Initializing the InfoWindow, Map and LatLngBounds objects.
	     $scope.InfoWindow = new google.maps.InfoWindow();
	     $scope.Latlngbounds = new google.maps.LatLngBounds();
	     $scope.Map = new google.maps.Map(document.getElementById("duserMap"), $scope.MapOptions);
	
	     //Looping through the Array and adding Markers.
	     for (var i = 0; i < $scope.appointmentdata.length; i++) {
	         var data = $scope.appointmentdata[i];
	         var myLatlng = new google.maps.LatLng(latitude, longitude);
	
	         //Initializing the Marker object.
	         var marker = new google.maps.Marker({
	             position: myLatlng,
	             map: $scope.Map,
	             animal_category: data.animal_category,
	             icon:icon
	         });
	
	         //Adding InfoWindow to the Marker.
	         (function (marker, data) {
	             google.maps.event.addListener(marker, "click", function (e) {
	                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.address + "</div>");
	                 $scope.InfoWindow.open($scope.Map, marker);
	             });
	         })(marker, data);
	
	         //Plotting the Marker on the Map.
	         $scope.Latlngbounds.extend(marker.position);
	     }
	
	     //Adjusting the Map for best display.
	     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
	     $scope.Map.fitBounds($scope.Latlngbounds);
		});
	
	}
	
	
	//for get appointment count
	$http.get("./rest/registration/getappointcount/"+$scope.doctid).then(function(response){
		$scope.getappointmentcount = response.data;
		$scope.getcount = $scope.getappointmentcount[0].count; 
	});
	
	$scope.appointpage = function(){	
		window.location.href="#/docappointment/"+$scope.doctid;
	}
	
	//getting data for appointment 
	$http.get("./rest/doctordata/appointments/"+$scope.doctid+"/"+"Customer"+"/"+"seen"+"/"+"Pending").then(function(response){
		$scope.appointmentdata = response.data;
		$scope.appcount = $scope.appointmentdata.length; 
		var someStr = $scope.appointmentdata;
		var accept = someStr.replace(/['"]+/g, '')
		if(accept == "INTERNAL_SERVER_ERROR"){
			$scope.appcount = "0";
		}
		
	});
	
	$scope.appointclk = function(data){
		$scope.petview = true;
		$scope.pet_category = data.animal_category;
		$scope.pet_name = data.animal_name;
		$scope.addresses = data.address;
		$scope.age = data.age;
		$scope.deviceid = data.uid;
		$scope.datetime = data.dateandtime;
		$scope.appid = data.appointmentid;
		$http.get("./rest/doctordata/getallimages/"+$scope.appid).then(function(response){
			$scope.getallpetimg = response.data;
			$scope.getvideo = $scope.getallpetimg[0].pet_videos; 
		});
	}
	
	$scope.showschedule = function() {
		$scope.scheduleform = true;
	}
	
	$scope.approved = function(){
		var dates = $('#datetimepickers').val();
		$scope.docdatetime = dates;
		var dateval = $scope.docdatetime.split("/");
		$scope.updatedate = dateval[1]+"/"+dateval[0]+"/"+dateval[2];
		
		$scope.request = "Confirmed";
		$scope.customerstatus ="Not Shown"
		var data ={
			appointmentid:$scope.appid,
			//dateandtime:$scope.datetime,
			docappointmentdate:$scope.updatedate,
			request:$scope.request,
			customerstatus:$scope.customerstatus
		}
		$http.post("./rest/doctordata/doctorappointment",data).then(function(response){
			$scope.confirmappointment = response.data;
			alert("Sent confirmation");
			window.location.reload();
		});
	}
	
	
	//For Notification count and view
	$http.get("./rest/doctordata/appointnotification/"+$scope.doctid+"/"+"unseen").then(function(response){
		$scope.notification = response.data;
		$scope.notifycount = $scope.notification.length;
		var someStr = $scope.notification;
		var accept = someStr.replace(/['"]+/g, '')
		if(accept == "INTERNAL_SERVER_ERROR"){
			$scope.notifycount = "0";
		}
		
		
	});
	
	//Update the notification status
	$scope.notificationupdate=function(data){
		$scope.status = data.status;
		if(data.status=="unseen"){
			$scope.status = "seen"
			var data={
				appointmentid:data.appointmentid,
				status:$scope.status
		     }
			$http.post("./rest/doctordata/statusupdate/",data).then(function(response){
				$scope.updatenotification = response.data;
				window.location.href="#/docappointment/"+$scope.doctid
			});
		}
	}
	
	//Getting the Approved customer List
	$scope.approvedview = function(){
		window.location.href="#approvedcustomerlist/"+$scope.doctid;
		window.location.reload();
	}
	
	
	
	$http.get("./rest/doctordata/getapprovedList/"+$scope.doctid+"/"+"Customer"+"/"+"Confirmed").then(function(response){
		$scope.getapprovedlist = response.data;
	});
	
	//Getting the Approved customer List
	$scope.medapprovedview = function(){
		window.location.href="#approvedmedlist/"+$scope.doctid;
		window.location.reload();
	}
	
	$http.get("./rest/doctordata/getmedapprovedList/"+$scope.doctid+"/"+"Medical Supplier"+"/"+"Confirmed").then(function(response){
		$scope.getmedicalapprovedlist = response.data;
	});
	
	
	
	
	//Edit approved date
	$scope.editdate = function(approvedlist){
		$scope.viewdate = true;
		$scope.editapproveddate = approvedlist.docappointmentdate;
		$scope.appid = approvedlist.appointmentid;
		$scope.customername = approvedlist.customer_name;
		$scope.medname = approvedlist.med_name;
	}
	
	$scope.updatedate = function(){
		$scope.editdata = $('#approveddatetimepicker').val();
		var dateval = $scope.editdata.split("/");
		$scope.aligndate = dateval[1]+"/"+dateval[0]+"/"+dateval[2];
		
		var data = {
			appointmentid:$scope.appid,
			docappointmentdate:$scope.aligndate,
		}
		$http.post("./rest/doctordata/updateapproveddate",data).then(function(response){
			$scope.saveeditdate = response.data;
			var someStr = $scope.saveeditdate;
			var accept = someStr.replace(/['"]+/g, '')
			
			if(accept == "ACCEPTED"){
				alert("Updated Successfully");
				window.location.reload();
			}
		});
	}
	
	

	
	
	//----------------------------------
	
	//For medical side notification
	
	//Getting medical supporter notification data
	$http.get("./rest/medsupport/medappointnotification/"+$scope.doctid+"/"+"unseen").then(function(response){
		$scope.mednotification = response.data;
		$scope.mednotifycount = $scope.mednotification.length;
		var someStr = $scope.mednotification;
		var accept = someStr.replace(/['"]+/g, '')
		if(accept == "INTERNAL_SERVER_ERROR"){
			$scope.mednotifycount = "0";
		}
	});
	
	$scope.notificationmedupdate=function(data){
		$scope.status = data.status;
		if(data.status=="unseen"){
			$scope.status = "seen"
			var data={
				appointmentid:data.appointmentid,
				status:$scope.status
		     }
			$http.post("./rest/doctordata/statusupdate/",data).then(function(response){
				$scope.updatemednotification = response.data;
				window.location.href="#/medappointmentlist/"+$scope.doctid
			});
		}
			
	}
	
	$http.get("./rest/medsupport/medappointments/"+$scope.doctid+"/"+"Medical Supplier"+"/"+"seen").then(function(response){
		$scope.medappointmentdata = response.data;
	});
	
	
	$scope.medappointclk = function(data)
	{
		$scope.petview = true;
		$scope.appid = data.appointmentid;
		$scope.medname = data.med_name;
		$scope.email = data.med_email;
		$scope.ph = data.med_ph;
		$scope.address = data.med_address;
		$scope.time = data.dateandtime;
	}
	
	
	
	//save event announcement
	$scope.savebtn = function(){
		
		var dates = $('#datetimepicker').val();
		$scope.eventdate = dates;
		var dateval = $scope.eventdate.split("/");
		
		$scope.aligndate = dateval[1]+"/"+dateval[0]+"/"+dateval[2];
		
		var data ={
				event_id:0,
				eventname:$scope.eventname,
				date:$scope.aligndate,
				venue:$scope.venue,
				contact:$scope.contactno,
				description:$scope.description,
  		}
		updateimg(data);
		
	} 
	
	$scope.saveevent = function(data){
		$http.post("./rest/doctordata/events",data).then(function(response){
			$scope.saveeventreg = response.data;
			var someStr = $scope.saveeventreg;
			var accepted = someStr.replace(/['"]+/g, '')
			if(accepted == "ACCEPTED"){
				alert("Successfully Inserted event Registration");
				$scope.eventname = "";
				$scope.aligndate = "";
				$scope.venue = "";
				$scope.contactno = "";
				$scope.description = "";
			}
		});
	}
	
		
	
	 var updateimg = function(data){
			var files = angular.element(document.querySelector('.imgupload'));
			if(files.val()!= 0){
				var formdata = new FormData();
			 	for(var i=0;i<files.length;i++){ 
			 		var fileObj= files[i].files;
			 		formdata.append("files" ,fileObj[0]);   
			    }
			 	
			 	 var xhr = new XMLHttpRequest();    
			 	 xhr.open("POST","./rest/file/upload/drproimg");
			 	xhr.send(formdata);
			    	xhr.onload = function(e) {
			    		if (this.status == 200) {
			    			//$scope.tmppath;
			    			var obj = JSON.parse(this.responseText);
			    			
			    			var imgpath = obj[0].path;
			    			
			    			data.eventlogo = imgpath;
			    			
			    			if(data.event_id == 0){
			    				$scope.saveevent(data);
			    			}else{
			    				
			    			}
		    		    }
			    	};
			}else{
				$scope.saveevent(data);
			}
		};
		
}).directive('myCustomUrl', function() {
    return {
        templateUrl: './ng-view/common/doctor-header.html'
      };
   });