app.controller('mapCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$compile) {
	
	
	 $scope.cid = window.location.hash;
	 var customerid =$scope.cid.split("/");
	 var cid = customerid[2];
	   
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
			$scope.getanimallist = response.data;
			getpetmap();
			//boundarymap();
			//getusermap();
	 });
	 
     //Setting the Map options.
	 
	 function getpetmap(){
		angular.forEach($scope.getanimallist, function(value, key){
			
			 $scope.petlocation = value.address
			
			 var geocoder = new google.maps.Geocoder();
			 var address = $scope.petlocation; 
			 var results = $scope.getanimallist;
			   
			 geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					for(var i =0;i<results.length;i++){
						var latitude = results[i].geometry.location.lat();
						var longitude = results[i].geometry.location.lng();
					}
			    } 
			
				
			     $scope.MapOptions = {
			         center: new google.maps.LatLng(latitude, longitude),
			         zoom: 4,
			         mapTypeId: google.maps.MapTypeId.ROADMAP
			     };
			     
			     var icon = { 
			  		    url: './images/cow.png'                             
			  	 };
		     
			     //Initializing the InfoWindow, Map and LatLngBounds objects.
			     $scope.InfoWindow = new google.maps.InfoWindow();
			     $scope.Latlngbounds = new google.maps.LatLngBounds();
			     $scope.Map = new google.maps.Map(document.getElementById("dvMap"), $scope.MapOptions);
			
			     //Looping through the Array and adding Markers.
			     for (var i = 0; i < $scope.getanimallist.length; i++) {
			         var data = $scope.getanimallist[i];
			         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
			
			         //Initializing the Marker object.
			         var marker = new google.maps.Marker({
			             position: myLatlng,
			             map: $scope.Map,
			             animal_category: data.animal_category,
			             icon:icon
			         });
			
			         //Adding InfoWindow to the Marker.
			         (function (marker, data) {
			             google.maps.event.addListener(marker, "click", function (e) {
			                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
			                 $scope.InfoWindow.open($scope.Map, marker);
			             });
			         })(marker, data);
			
			         //Plotting the Marker on the Map.
			         $scope.Latlngbounds.extend(marker.position);
			     }
		
			     //Adjusting the Map for best display.
			     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
			     $scope.Map.fitBounds($scope.Latlngbounds);
			     
				 });
		 });
	 }
	 
	 
	 
	 //Pet boundary map
	 
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
		$scope.getanimallist = response.data;	
		
		$scope.uniques = _.map(_.groupBy($scope.getanimallist,function(doc){
			  return doc.animal_category;
		}),function(grouped){
			  return grouped[0];
		});
	});
	 
	 $scope.getanimal = function(animalcategory){
		$scope.eeee = animalcategory;
		$http.get("./rest/registration/getindividualpet/"+animalcategory+"/"+cid).then(function(response){
			$scope.getindividuallist = response.data;
			$scope.getanimalname = function(did){
				$scope.getpetname= _.where($scope.getindividuallist, {animalid:did});				
			}
		});
	 }
	 
	
	 
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
			$scope.getanimallist = response.data;
			$scope.petcount = $scope.getanimallist.length;
			
			$scope.viewmapcircle = function(){
				boundarymap();
			}
			$scope.getmeter = function(){
				$scope.getkm;
			}
	 });
	 
	 
	 
	 /*function boundarymap(){
			angular.forEach($scope.getanimallist, function(value, key){
				
				 $scope.petlocation = value.address
				
				 var geocoder = new google.maps.Geocoder();
				 var address = $scope.petlocation; 
				 var getpetlocation = $scope.getanimallist;
				 var results= _.pluck(getpetlocation,'address');
				   
				 geocoder.geocode( { 'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						for(var i =0;i<results.length;i++){
							var latitude = results[i].geometry.location.lat();
							var longitude = results[i].geometry.location.lng();
										
					     $scope.MapOptions = {
					         center: new google.maps.LatLng(latitude, longitude),
					         zoom: 10,
					         mapTypeId: google.maps.MapTypeId.ROADMAP
					     };
					  }
					} 
				  });
				 
				     var icon = { 
				  		    url: './images/cow.png'                             
				  	 };
				     
				     //Initializing the InfoWindow, Map and LatLngBounds objects.
				     $scope.InfoWindow = new google.maps.InfoWindow();
				     $scope.Latlngbounds = new google.maps.LatLngBounds();
				     $scope.Map = new google.maps.Map(document.getElementById("boundarymap"), $scope.MapOptions);
    			
				      
				     
				     var marker = new google.maps.Marker({
					      position: {lat: 9.91966, lng: 78.11939},
					      map: $scope.Map,          
					      draggable: true,
					      title: 'Animal marker'
					  });
					  
					  marker.addListener('dragend', function(event){
					      var a=cityCircle.getBounds();
					      if(!a.contains(event.latLng))
					      alert("Your Animal Going Outside From Boundry Range");                
					  });
					         	
					   var cityCircle ="";

					  google.maps.event.addListener($scope.Map, 'click', function(event){
					       cityCircle = new google.maps.Circle({
						     strokeColor: '#FF0000',
						     strokeOpacity: 0.8,
						     strokeWeight: 2,
						     fillColor: '#ffffff',
						     fillOpacity: 0.1,
						     map: $scope.Map,
						     center: event.latLng,
						     radius: 1000 * $scope.getkm,
						     draggable: true,
						     geodesic: true,
						     editable: true     
					       });
					       marker.setPosition(event.latLng);
					    });

					   google.maps.event.addListener($scope.Map, 'click', function(event){                
						     var oldLatLng = marker.getPosition();         
						     var a=cityCircle.getBounds();
						     marker.setPosition(event.latLng);
						     if(!a.contains(event.latLng))
						     alert("Your Animal Going Outside From Boundry Range");                                
					    });
					   
					   document.getElementById('save_boundary').addEventListener('click', function(){
						   var location = $scope.location;
						   var geocoder = new google.maps.Geocoder();
						   geocoder.geocode( { 'location': location}, function(results, status) {
									if (status == google.maps.GeocoderStatus.OK) {
										for(var i =0;i<results.length;i++){
											$scope.lat = results[i].geometry.location.lat();
											$scope.lng = results[i].geometry.location.lng();
														
									     $scope.MapOptions = {
									         center: new google.maps.LatLng($scope.lat, $scope.lng),
									         zoom: 10,
									         mapTypeId: google.maps.MapTypeId.ROADMAP
									     };
									  }
										//alert("Center latitude of Boundary :"+$scope.lat);
										// alert("Center longitude of Boundary :"+$scope.lng);
									} 
							});
						     
						 // var radius=parseInt(cityCircle.getRadius());
						  
						  //alert("Radius Value of Boundary  :"+radius);
						  //$scope.radius = radius;
						  
						  var latLng = new google.maps.LatLng($scope.lat, $scope.lng);
						  geocoder.geocode({
						      latLng: latLng
						    },
						    function(responses) {
						    if (responses && responses.length > 0)
						    {
						       $scope.address = responses[0].formatted_address;
						    } else {
						      alert('Cannot determine address at this location.');
						    }
						  });

						  
						  
						  var data = {
								 pet_boundariesid:0,
								 latitude:$scope.latitude,
								 longitude:$scope.longitude,
								 boundary_radius:$scope.radius,
								 address:$scope.address
						  }
						  
						  $http.post("./rest/registration/petboundary",data).then(function(response){
							 $scope.saveboundaries = response.data; 
						  });
						  
					  });
					   
					$scope.removeLine = function(){
						alert("ok");
						cityCircle.setMap($scope.Map, null);
					}
					   
					  
			     //Looping through the Array and adding Markers.
			     for (var i = 0; i < $scope.getanimallist.length; i++) {
			    	 var data = $scope.getanimallist[i];
			         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
			
			         //Initializing the Marker object.
			         var markers = new google.maps.Marker({
			             position: myLatlng,
			             map: $scope.Map,
			             animal_category: data.animal_category,
			             icon:icon
			         });
			
			         //Adding InfoWindow to the Marker.
			         (function (markers, data) {	
			             google.maps.event.addListener(marker, "click", function (e) {
			                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
			                 $scope.InfoWindow.open($scope.Map, markers);
			             });
			         })(markers, data);
			
			         //Plotting the Marker on the Map.
			         $scope.Latlngbounds.extend(markers.position);
			     }
			
			     //Adjusting the Map for best display.
			     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
			     $scope.Map.fitBounds($scope.Latlngbounds);
				 
			 });
	     	     
		 }*/

	function boundarymap() {        
    	
    	angular.forEach($scope.getanimallist, function(value, key){
			
			 $scope.petlocation = value.address
			
			 var geocoder = new google.maps.Geocoder();
			 var address = $scope.petlocation; 
			 var getpetlocation = $scope.getanimallist;
			 var results = _.pluck(getpetlocation,'address');
			   
			 geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					for(var i =0;i<results.length;i++){
						var latitude = results[i].geometry.location.lat();
						var longitude = results[i].geometry.location.lng();
									
				     $scope.MapOptions = {
				         center: new google.maps.LatLng(latitude, longitude),
				         zoom: 14,
				         mapTypeId: google.maps.MapTypeId.ROADMAP
				     };
				  }
				} 
			 });
			 
			 var icon = { 
			     url: './images/cow.png'                             
			 };
			   
			 $scope.InfoWindow = new google.maps.InfoWindow();
			 $scope.Latlngbounds = new google.maps.LatLngBounds();
			 $scope.Map = new google.maps.Map(document.getElementById("createcirclemap"), $scope.MapOptions);

			 //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getanimallist.length; i++) {
		    	 var data = $scope.getanimallist[i];
		         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
		
		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             animal_category: data.animal_category,
		             icon:icon
		         });
		
		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {	
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);
		
		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }
		     
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
   	  });
    }
    
    
      function circleview(){
	    	 var markers = new google.maps.Marker({	
					 position: {lat: $scope.latitude, lng: $scope.longitude},
					 map: $scope.Map,          
					 draggable: true,
					 title: 'Animal marker'
			 });
				 
			 markers.addListener('dragend', function(event) 
			 {
	             var a=cityCircle.getBounds();
	             if(!a.contains(event.latLng))
	             alert("Your Animal Going Outside From Boundry Range");                
			 });
	   
			 var cityCircle = new google.maps.Circle({
	             strokeColor: '#FF0000',
	             strokeOpacity: 0.8,
	             strokeWeight: 2,
	             fillColor: '#ffffff',
	             fillOpacity: 0.1,
	             map: $scope.Map,
	             center: {lat: $scope.latitude, lng: $scope.longitude},
	             radius: 1000 * $scope.getkm,
	            // draggable: true,
	            // geodesic: true,
	             editable: true     
	        });
			 
			 	
		     cityCircle.addListener('click', function(event) 
		     {
		         markers.setPosition(event.latLng);
		     });
	      
	      
		     var allcircle = [];
		     
		    
		   /* google.maps.event.addListener('click', function() {
		    	$scope.removeLine = function(){
		    		markers.setMap(null);
		    	}
		    });*/
		    	 		     
		     google.maps.event.addListener($scope.Map, 'click', function(event) 
		     {                
	             var oldLatLng = markers.getPosition();         
	             var a=cityCircle.getBounds();
	             markers.setPosition(event.latLng);
	             if(!a.contains(event.latLng))
	            	 alert("Your Animal Going Outside From Boundry Range");                                
		     });
		   
		     var center=cityCircle.getCenter();
		     var radius=parseInt(cityCircle.getRadius());
    	}
    
    
    
    	$scope.setcircle =function(){
    	
 	    var geocoder = new google.maps.Geocoder();
 	    var locate = $scope.locate;

 	    geocoder.geocode( { 'address': locate}, function(results, status) {
				 if (status == google.maps.GeocoderStatus.OK) {
					for(var i =0;i<results.length;i++){
						$scope.latitude = results[i].geometry.location.lat();
						$scope.longitude = results[i].geometry.location.lng();

	   					$scope.MapOptions = {
						    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
						    zoom: 10,
						    mapTypeId: google.maps.MapTypeId.ROADMAP
						};
	   					circleview();
					}
					
					var data = {
							 pet_boundariesid:0,
							 latitude:$scope.latitude,
							 longitude:$scope.longitude,
							 boundary_radius:$scope.getkm,
							 address:$scope.locate,
							 customer_id:cid
					  }
					  
					  $http.post("./rest/registration/petboundary",data).then(function(response){
						 $scope.saveboundaries = response.data; 
						 var someStr = $scope.saveboundaries;
						 var accept = someStr.replace(/['"]+/g, '')
						 if(accept == "ACCEPTED"){
							$scope.locate="";
							$scope.getkm="";
						 }
					  });
				 } 
    	 });
    }
    	
    	
   
    //View boundary circle
    	
    $scope.createcircle = true;
    
    $scope.viewcircle = function(){
    	$http.get("./rest/registration/getcirclelist/"+cid).then(function(response){
        	$scope.getcircleview = response.data;
        	getpetcircle();
        	$scope.viewcircles=true;
        	$scope.createcircle = false;
        })	
    }
    
    
    $scope.backtocircle = function(){
    	$scope.createcircle = true;
    	$scope.viewcircles=false;
    	boundarymap();
    }
    
    function getpetcircle(){
    	
    	angular.forEach($scope.getanimallist, function(value, key){
			
			 $scope.petlocation = value.address
			
			 var geocoder = new google.maps.Geocoder();
			 var address = $scope.petlocation; 
			 var getpetlocation = $scope.getanimallist;
			 var results = _.pluck(getpetlocation,'address');
			   
			 geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					for(var i =0;i<results.length;i++){
						var latitude = results[i].geometry.location.lat();
						var longitude = results[i].geometry.location.lng();
									
				     $scope.MapOptions = {
				         center: new google.maps.LatLng(latitude, longitude),
				         zoom: 14,
				         mapTypeId: google.maps.MapTypeId.ROADMAP
				     };
				  }
				} 
			 });
			 
			 var icon = { 
			     url: './images/cow.png'                             
			 };
			   
			 $scope.InfoWindow = new google.maps.InfoWindow();
			 $scope.Latlngbounds = new google.maps.LatLngBounds();
			 $scope.Mapcircle = new google.maps.Map(document.getElementById("viewcircle"), $scope.MapOptions);

			 //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getanimallist.length; i++) {
		    	 var data = $scope.getanimallist[i];
		         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
		
		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Mapcircle,
		             animal_category: data.animal_category,
		             icon:icon
		         });
		
		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {	
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
		                 $scope.InfoWindow.open($scope.Mapcircle, marker);
		             });
		         })(marker, data);
		
		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }
         
   	  });
    	
    	  // Create the map.
    			 
			 var circlemap = $scope.getcircleview;
			 var infowindow = new google.maps.InfoWindow();

			 var marker, i;
			 var markers = new Array();	 
			 			 
			 for (var i = 0; i < $scope.getcircleview.length; i++) {
				 
			    	var data = $scope.getcircleview[i];
			        var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
			    
			        marker = new google.maps.Circle({
			             strokeColor: '#FF0000',
			             strokeOpacity: 0.8,
			             strokeWeight: 2,
			             fillColor: '#ffffff',
			             fillOpacity: 0.1,
			             map: $scope.Mapcircle,
			             center: myLatlng,
			             radius: 1000 * $scope.getcircleview[i].boundary_radius,
			           //  draggable: true,
			           //  geodesic: true,
			            // editable: true     
			     });
			     
			        marker.setMap($scope.Mapcircle);
			     
			     //obj[i] = new google.maps.Circle(cityCircle);		     
			     //x++;
			     
			     google.maps.event.addListener(marker,'click',(function(marker,i){
			         return function() {
			             infowindow.setContent($scope.getcircleview[i].pet_boundariesid);
			             var pid = $scope.getcircleview[i].pet_boundariesid;
			             
			             var result = confirm("Want to Delete this Circle?");
			             if(result){
			            	$http.post("./rest/registration/removecircle/"+pid).then(function(response){
					    		$scope.removecircle = response.data; 
					    		var someStr = $scope.removecircle;
								var accept = someStr.replace(/['"]+/g, '')
								if(accept == "ACCEPTED"){
									marker.setMap(null);
								}
						    });
			             }else{
			            	 alert("Canceled the Delete Option");
			             }
			           }
			     })(marker,i));	     		     
			    
			     /*google.maps.event.addListener(cityCircle, 'click', function(x) {
			    	 var pid = $scope.getcircleview[x].boundary_radius;
			    	 alert("Ok va"+pid);
			    	 var pid = data.pet_boundariesid;
			    	 $http.post("./rest/registration/removecircle/"+pid).then(function(response){
			    		$scope.removecircle = response.data; 
			    	 });
			         this.setMap(null);
			     });*/
			     
			     $scope.MapOptions = {
					center: new google.maps.LatLng($scope.getcircleview[i].latitude, $scope.getcircleview[i].longitude),
					zoom: 10,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
			     
			 }
		
		     //Adjusting the Map for best display.
		     $scope.Mapcircle.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Mapcircle.fitBounds($scope.Latlngbounds);
		
	 }
    
	 
});