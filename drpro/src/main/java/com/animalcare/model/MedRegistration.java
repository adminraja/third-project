package com.animalcare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Medical_supplier")
public class MedRegistration {
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private int med_id;
	private String med_name; 
	private String med_address;
	private String med_ph;
	private String med_email;
	private String med_pwd;
	private String typeofreg;
	
	public int getMed_id() {
		return med_id;
	}
	public void setMed_id(int med_id) {
		this.med_id = med_id;
	}
	public String getMed_name() {
		return med_name;
	}
	public void setMed_name(String med_name) {
		this.med_name = med_name;
	}
	public String getMed_address() {
		return med_address;
	}
	public void setMed_address(String med_address) {
		this.med_address = med_address;
	}
	public String getMed_ph() {
		return med_ph;
	}
	public void setMed_ph(String med_ph) {
		this.med_ph = med_ph;
	}
	public String getMed_email() {
		return med_email;
	}
	public void setMed_email(String med_email) {
		this.med_email = med_email;
	}
	public String getMed_pwd() {
		return med_pwd;
	}
	public void setMed_pwd(String med_pwd) {
		this.med_pwd = med_pwd;
	}
	public String getTypeofreg() {
		return typeofreg;
	}
	public void setTypeofreg(String typeofreg) {
		this.typeofreg = typeofreg;
	}
	
	
	
}
