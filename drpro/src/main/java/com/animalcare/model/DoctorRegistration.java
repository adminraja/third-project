package com.animalcare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vetdoctor_table")
public class DoctorRegistration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private int doctorid; 
	private String doctor_name;
	private String doctor_email;
	private String doctor_phoneno;
	private String password;
	
	//Before that name were location
	private String street;
	private String typeofreg;
	private String dateandtime;
	private String doctor_status;
	private String latitude;
	private String longitude;
	private String city;
	
	public int getDoctorid() {
		return doctorid;
	}
	public void setDoctorid(int doctorid) {
		this.doctorid = doctorid;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getDoctor_email() {
		return doctor_email;
	}
	public void setDoctor_email(String doctor_email) {
		this.doctor_email = doctor_email;
	}
	public String getDoctor_phoneno() {
		return doctor_phoneno;
	}
	public void setDoctor_phoneno(String doctor_phoneno) {
		this.doctor_phoneno = doctor_phoneno;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTypeofreg() {
		return typeofreg;
	}
	public void setTypeofreg(String typeofreg) {
		this.typeofreg = typeofreg;
	}
	public String getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(String dateandtime) {
		this.dateandtime = dateandtime;
	}
	public String getDoctor_status() {
		return doctor_status;
	}
	public void setDoctor_status(String doctor_status) {
		this.doctor_status = doctor_status;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
