package com.animalcare.vo;

public class CattleVO {
	
	private int cattleid;
	private String petname;
	private String pet_category;
	private String cattle_price;
	private String cattle_image;
	private String sellerid;
	
	public int getCattleid() {
		return cattleid;
	}
	public void setCattleid(int cattleid) {
		this.cattleid = cattleid;
	}
	public String getPetname() {
		return petname;
	}
	public void setPetname(String petname) {
		this.petname = petname;
	}
	public String getPet_category() {
		return pet_category;
	}
	public void setPet_category(String pet_category) {
		this.pet_category = pet_category;
	}
	public String getCattle_price() {
		return cattle_price;
	}
	public void setCattle_price(String cattle_price) {
		this.cattle_price = cattle_price;
	}
	public String getCattle_image() {
		return cattle_image;
	}
	public void setCattle_image(String cattle_image) {
		this.cattle_image = cattle_image;
	}
	public String getSellerid() {
		return sellerid;
	}
	public void setSellerid(String sellerid) {
		this.sellerid = sellerid;
	}
  
	

}