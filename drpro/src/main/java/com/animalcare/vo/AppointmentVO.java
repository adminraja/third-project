package com.animalcare.vo;

public class AppointmentVO {

	private int appointmentid;
	private String doctorid; 
	private String dateandtime;
	private String customer_id;
	private String request;
	private String type;
	private String animalid;
	private String docappointmentdate;
	private String status;
	private String customerstatus;
	private String med_id;
	private String pet_image;
	private String pet_images;
	private String pet_videos;

	public int getAppointmentid() {
		return appointmentid;
	}
	public void setAppointmentid(int appointmentid) {
		this.appointmentid = appointmentid;
	}
	public String getDoctorid() {
		return doctorid;
	}
	public void setDoctorid(String doctorid) {
		this.doctorid = doctorid;
	}
	public String getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(String dateandtime) {
		this.dateandtime = dateandtime;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAnimalid() {
		return animalid;
	}
	public void setAnimalid(String animalid) {
		this.animalid = animalid;
	}
	public String getDocappointmentdate() {
		return docappointmentdate;
	}
	public void setDocappointmentdate(String docappointmentdate) {
		this.docappointmentdate = docappointmentdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerstatus() {
		return customerstatus;
	}
	public void setCustomerstatus(String customerstatus) {
		this.customerstatus = customerstatus;
	}
	public String getMed_id() {
		return med_id;
	}
	public void setMed_id(String med_id) {
		this.med_id = med_id;
	}
	public String getPet_image() {
		return pet_image;
	}
	public void setPet_image(String pet_image) {
		this.pet_image = pet_image;
	}
	public String getPet_images() {
		return pet_images;
	}
	public void setPet_images(String pet_images) {
		this.pet_images = pet_images;
	}
	public String getPet_videos() {
		return pet_videos;
	}
	public void setPet_videos(String pet_videos) {
		this.pet_videos = pet_videos;
	}
	
	
}
