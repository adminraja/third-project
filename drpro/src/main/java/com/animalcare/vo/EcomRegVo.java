package com.animalcare.vo;

public class EcomRegVo{
	
 private int sellerid;
 private String sellername;
 private String sellerstreet;
 private String sellerphone;
 private String sellershop;
 private String selleremail;
 private String sellerpassword;
 private String typeofreg;
	
 public String getTypeofreg() {
	return typeofreg;
}
public void setTypeofreg(String typeofreg) {
	this.typeofreg = typeofreg;
}
public int getSellerid() {
		return sellerid;
	}
	public void setSellerid(int sellerid) {
		this.sellerid = sellerid;
	}
	public String getSellername() {
		return sellername;
	}
	public void setSellername(String sellername) {
		this.sellername = sellername;
	}
	public String getSellerstreet() {
		return sellerstreet;
	}
	public void setSellerstreet(String sellerstreet) {
		this.sellerstreet = sellerstreet;
	}
	public String getSellerphone() {
		return sellerphone;
	}
	public void setSellerphone(String sellerphone) {
		this.sellerphone = sellerphone;
	}
	public String getSellershop() {
		return sellershop;
	}
	public void setSellershop(String sellershop) {
		this.sellershop = sellershop;
	}
	public String getSelleremail() {
		return selleremail;
	}
	public void setSelleremail(String selleremail) {
		this.selleremail = selleremail;
	}
	public String getSellerpassword() {
		return sellerpassword;
	}
	public void setSellerpassword(String sellerpassword) {
		this.sellerpassword = sellerpassword;
	}
	
	
}