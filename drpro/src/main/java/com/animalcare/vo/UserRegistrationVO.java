package com.animalcare.vo;

public class UserRegistrationVO {

	private int customer_id;
	private String customer_name;
	private String customer_street;
	private String customer_phone;
	private String customer_email;
	private String password;
	private String uid;
	private String typeofreg;
	private String userphoto;
	private String doctorappointment;
	private String city;

	
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getCustomer_street() {
		return customer_street;
	}
	public void setCustomer_street(String customer_street) {
		this.customer_street = customer_street;
	}
	public String getCustomer_phone() {
		return customer_phone;
	}
	public void setCustomer_phone(String customer_phone) {
		this.customer_phone = customer_phone;
	}
	public String getCustomer_email() {
		return customer_email;
	}
	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getTypeofreg() {
		return typeofreg;
	}
	public void setTypeofreg(String typeofreg) {
		this.typeofreg = typeofreg;
	}
	public String getUserphoto() {
		return userphoto;
	}
	public void setUserphoto(String userphoto) {
		this.userphoto = userphoto;
	}
	public String getDoctorappointment() {
		return doctorappointment;
	}
	public void setDoctorappointment(String doctorappointment) {
		this.doctorappointment = doctorappointment;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
    	
}
