package com.animalcare.helper;

import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.type.TimestampType;
import com.animalcare.vo.EcomRegVo;
import com.animalcare.model.EcomProductInsert;
import com.animalcare.model.EcomRegDetails;
import com.animalcare.service.EcomRegService;
import com.sun.jersey.api.core.InjectParam;
import com.animalcare.vo.ProductVO;
import com.animalcare.vo.CattleVO;
import com.animalcare.model.EcomCattleInsert; 



public class EcomRegHelper  {

	@InjectParam
	EcomRegService ecomregservice;

	
	
	public int saveEcomRegContent(EcomRegVo ecomregvo){
		int id=0;
		try{
			Date date =new Date();
			
			EcomRegDetails ecomregdetails = new EcomRegDetails();
			
			ecomregdetails.setSellerid(ecomregvo.getSellerid());
			ecomregdetails.setSellername(ecomregvo.getSellername());
			ecomregdetails.setSellerphone(ecomregvo.getSellerphone());
			ecomregdetails.setSellershop(ecomregvo.getSellershop());
			ecomregdetails.setSellerstreet(ecomregvo.getSellerstreet());
            ecomregdetails. setSelleremail(ecomregvo.getSelleremail());
            ecomregdetails.setSellerpassword(ecomregvo.getSellerpassword());
            ecomregdetails.setTypeofreg(ecomregvo.getTypeofreg());
            
			id=ecomregservice.ecomRegcontent(ecomregdetails);
			         
			
			}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	public int saveEcomProductInsert(ProductVO productvo){
		
		int id=0;
		try{
			Date date =new Date();
			EcomProductInsert ecomproductinsert = new EcomProductInsert();
			
			ecomproductinsert.setProduct_id(productvo.getProduct_id());
			ecomproductinsert.setProduct_name(productvo.getProduct_name());
			ecomproductinsert.setProduct_category(productvo.getProduct_category());
			ecomproductinsert.setProduct_price(productvo.getProduct_price());
			ecomproductinsert.setSellerid(productvo.getSellerid());
			ecomproductinsert.setProduct_image(productvo.getProduct_image());
			ecomproductinsert.setProduct_offer(productvo.getProduct_offer());
			
		id=ecomregservice.ecomProductInsert(ecomproductinsert);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	
	return id;
	}
	
	
	public int saveEcomCattleInsert(CattleVO cattlevo){
		int id=0;
		try{
			Date date=new Date();
			EcomCattleInsert ecomcattleinsert =new EcomCattleInsert();
			ecomcattleinsert.setCattleid(cattlevo.getCattleid());
			ecomcattleinsert.setPetname(cattlevo.getPetname());
			ecomcattleinsert.setPet_category(cattlevo.getPet_category());
			ecomcattleinsert.setCattle_price(cattlevo.getCattle_price());
			ecomcattleinsert.setCattle_image(cattlevo.getCattle_image());
			ecomcattleinsert.setSellerid(cattlevo.getSellerid());
		    id=ecomregservice.ecomCattleInsert(ecomcattleinsert);
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return id;
	
	}
	
}
