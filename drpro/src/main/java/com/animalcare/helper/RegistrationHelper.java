package com.animalcare.helper;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.animalcare.model.AnimalRegisration;
import com.animalcare.model.Appointment;
import com.animalcare.model.CustomerProduct;
import com.animalcare.model.DoctorRegistration;
import com.animalcare.model.EventRegistration;
import com.animalcare.model.MedRegistration;
import com.animalcare.model.PetBoundaries;
import com.animalcare.model.Petimage;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.RegistrationService;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.CustomerProductVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.EventRegistrationVO;
import com.animalcare.vo.MedregRegistrationVO;
import com.animalcare.vo.PetBoundariesVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;


public class RegistrationHelper {
  
	@InjectParam
	RegistrationService registerservice;	
	public int saveregistration(UserRegistrationVO userregistrationvo){
		int id=0;
		UserRegistration userregister = new UserRegistration();
		getMsg(userregistrationvo);	
		userregister.setCustomer_name(userregistrationvo.getCustomer_name());
		userregister.setCustomer_street(userregistrationvo.getCustomer_street());
		userregister.setCustomer_phone(userregistrationvo.getCustomer_phone());
		userregister.setCustomer_email(userregistrationvo.getCustomer_email());
		userregister.setPassword(userregistrationvo.getPassword());
		userregister.setUid(userregistrationvo.getUid());
		userregister.setTypeofreg(userregistrationvo.getTypeofreg());
		userregister.setUserphoto(userregistrationvo.getUserphoto());
		userregister.setCity(userregistrationvo.getCity());
		id = registerservice.saveregistration(userregister);
		
		return id;
	}
	
	public Response getMsg(UserRegistrationVO userregistrationvo) {
		 
		try{
			
			UserRegistration userregister = new UserRegistration();
			
		    final String emildate = userregistrationvo.getCustomer_email();
		    System.out.println("Email data:"+ emildate);
		    String to=userregistrationvo.getCustomer_email();
		    final String from="murugan.karthik@kevellcorp.com";//change accordingly 
			final String password="karthi@47";//change accordingly 
			
			//Get the session object 
			Properties props = new Properties(); 
			props.put("mail.smtp.host", "smtp.gmail.com"); 
			props.put("mail.smtp.socketFactory.port", "465"); 
			props.put("mail.smtp.socketFactory.class", 
			"javax.net.ssl.SSLSocketFactory"); 
			props.put("mail.smtp.auth", "true"); 
			props.put("mail.smtp.port", "465"); 

			Session session = Session.getDefaultInstance(props, 
			new javax.mail.Authenticator() { 
				protected PasswordAuthentication getPasswordAuthentication() { 
				return new PasswordAuthentication(from,password); 
				} 
			}); 
			   
			
			String email=userregistrationvo.getCustomer_email();
			String pass=userregistrationvo.getPassword();
			
			MimeMessage message = new MimeMessage(session); 
			message.setFrom(new InternetAddress(from)); 
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to)); 
			message.setSubject("User Authentication.."); 
			message.setText("Hi, You are created customer account in Drpro"+"\n"+"\n"+"Usermail: "+email+"\n"+"Password:"+pass); 
			//send message 
			Transport.send(message); 
			
			System.out.println("message sent successfully");
			
			
		}
		    catch(MessagingException e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.BAD_REQUEST).build();
		}
		return Response.status(Status.OK).entity(Status.OK).build();
	}
	
	public int savepetregistration(AnimalRegisration animalregvo){
		int id=0;
		AnimalRegisration animalregisration = new AnimalRegisration();
			
		animalregisration.setAnimal_category(animalregvo.getAnimal_category());
		animalregisration.setAnimal_name(animalregvo.getAnimal_name());
		animalregisration.setLatitude(animalregvo.getLatitude());
		animalregisration.setLongitude(animalregvo.getLongitude());
		animalregisration.setGender(animalregvo.getGender());
		animalregisration.setAge(animalregvo.getAge());
		animalregisration.setAddress(animalregvo.getAddress());
		animalregisration.setUid(animalregvo.getUid());
		animalregisration.setCustomer_id(animalregvo.getCustomer_id());
		id = registerservice.savepetregistration(animalregisration);
		
		return id;
	}
	
	public int savedocregistration(DoctorRegistrationVO doctorregistrationvo){
		int id=0;
		DoctorRegistration doctorregistrataion = new DoctorRegistration();
	    doctorregistrataion.setDoctor_name(doctorregistrationvo.getDoctor_name());
	    doctorregistrataion.setDoctor_email(doctorregistrationvo.getDoctor_email());
	    doctorregistrataion.setDoctor_phoneno(doctorregistrationvo.getDoctor_phoneno());
	    doctorregistrataion.setStreet(doctorregistrationvo.getStreet());
	    doctorregistrataion.setPassword(doctorregistrationvo.getPassword());
	    doctorregistrataion.setTypeofreg(doctorregistrationvo.getTypeofreg());
	    doctorregistrataion.setLatitude(doctorregistrationvo.getLatitude());
	    doctorregistrataion.setLongitude(doctorregistrationvo.getLongitude());
	    doctorregistrataion.setCity(doctorregistrationvo.getCity());
		id = registerservice.savedocregistration(doctorregistrataion);
		
		return id;
	}
	
	//save event registration  saveeventregistration
	public int saveeventregistration(EventRegistrationVO eventregistrationvo){
		int id=0;
		EventRegistration eventregistration = new EventRegistration();
		eventregistration.setEventname(eventregistrationvo.getEventname());
		eventregistration.setDate(eventregistrationvo.getDate());
		eventregistration.setVenue(eventregistrationvo.getVenue());
		eventregistration.setDescription(eventregistrationvo.getDescription());
		eventregistration.setContact(eventregistrationvo.getContact());
		eventregistration.setEventlogo(eventregistrationvo.getEventlogo());
		id =registerservice.saveeventregistration(eventregistration);
		return id;
	}
	
	
	
	
	
	//Medical supplier registration
	public int savemedregistration(MedregRegistrationVO medicalregvo){
		int id=0;
		MedRegistration medregistrataion = new MedRegistration();
		medregistrataion.setMed_name(medicalregvo.getMed_name());
		medregistrataion.setMed_email(medicalregvo.getMed_email());
	    medregistrataion.setMed_ph(medicalregvo.getMed_ph());
	    medregistrataion.setMed_address(medicalregvo.getMed_address());
	    medregistrataion.setMed_pwd(medicalregvo.getMed_pwd());
	    medregistrataion.setTypeofreg(medicalregvo.getTypeofreg());
		id = registerservice.savemedregistration(medregistrataion);
		
		return id;
	}
	
	//For doctor appointment
	public int saveappointment(AppointmentVO appointmentvo){
		int id=0;
		try{
			Appointment appointment = new Appointment();
		    appointment.setRequest(appointmentvo.getRequest());
			appointment.setDateandtime(appointmentvo.getDateandtime());
			appointment.setDoctorid(appointmentvo.getDoctorid());
			appointment.setCustomer_id(appointmentvo.getCustomer_id());
			appointment.setType(appointmentvo.getType());
			appointment.setPet_image(appointmentvo.getPet_image());
			appointment.setAnimalid(appointmentvo.getAnimalid());
			appointment.setMed_id(appointmentvo.getMed_id());
			appointment.setStatus("unseen");   
			/*appointment.setDocappointmentdate(appointmentvo.getDocappointmentdate());*/
			id = registerservice.saveappointment(appointment);
			
			String imgArray[] = appointmentvo.getPet_images().split(",");
			String videoArray[] = appointmentvo.getPet_videos().split(",");
			System.out.println("Pet Image:"+appointmentvo.getPet_images());
			for(int i=0;i<imgArray.length;i++){
				for(int k=0;k<videoArray.length;k++){
					Petimage petimage = new Petimage();
					petimage.setPet_images(imgArray[i]);
					petimage.setPet_videos(videoArray[k]);
					appointment.setAppointmentid(id);
					System.out.println("Pet Id:"+id);
					petimage.setAppointment(appointment);
					registerservice.savepetimage(petimage);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	
	//For pet boundaries
	public int savepetboundary(PetBoundariesVO petboundariesvo){
		int id=0;
		PetBoundaries petboundaries = new PetBoundaries();
		petboundaries.setLatitude(petboundariesvo.getLatitude());
		petboundaries.setLongitude(petboundariesvo.getLongitude());
		petboundaries.setBoundary_radius(petboundariesvo.getBoundary_radius());
		petboundaries.setAddress(petboundariesvo.getAddress());
		petboundaries.setCustomer_id(petboundariesvo.getCustomer_id());
		id = registerservice.savepetboundary(petboundaries);
		return id;
	}
	
	//Save Customers Product
	public int savecustomerproduct(CustomerProductVO customerproductvo){
		int id=0;
		CustomerProduct customerproduct = new CustomerProduct();
		customerproduct.setProduct_category(customerproductvo.getProduct_category());
		customerproduct.setProduct_name(customerproductvo.getProduct_name());
		customerproduct.setPrice(customerproductvo.getPrice());
		customerproduct.setProduct_description(customerproductvo.getProduct_description());
		customerproduct.setCustomer_id(customerproductvo.getCustomer_id());
		customerproduct.setProduct_image(customerproductvo.getProduct_image());
		id=registerservice.savecustomerproduct(customerproduct);
		return id;
	}
	
}
