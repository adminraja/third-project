package com.animalcare.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.animalcare.model.AnimalRegisration;
import com.animalcare.model.Appointment;
import com.animalcare.model.CustomerProduct;
/*import com.animalcare.model.Appointment;
*/import com.animalcare.model.DoctorRegistration;
import com.animalcare.model.EventRegistration;
import com.animalcare.model.MedRegistration;
import com.animalcare.model.PetBoundaries;
import com.animalcare.model.Petimage;
import com.animalcare.model.UserRegistration;
import com.animalcare.util.HibernateUtil;


public class RegistrationService {

	
	public int saveregistration(UserRegistration userregistration){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(userregistration);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("saveregistration---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int savepetregistration(AnimalRegisration animalregvo){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(animalregvo);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("animalregisration---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int savedocregistration(DoctorRegistration doctorregistrtaion){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(doctorregistrtaion);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("doctorregistraion---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int saveeventregistration(EventRegistration eventregistration){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(eventregistration);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Event Registraion---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int savemedregistration(MedRegistration medregistration){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(medregistration);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Medregistration---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int saveappointment(Appointment appointment){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(appointment);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Appointment---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	public int savepetimage(Petimage petimage){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(petimage);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Petimage---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	//Save Pet Boundaries
	public int savepetboundary(PetBoundaries petboundaries){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(petboundaries);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("PetBoundaries---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	//Save customer's product 
	public int savecustomerproduct(CustomerProduct customerproduct){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(customerproduct);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Customer Product---"+e);
		}
		finally{
			session.close();
		}
		
		return (Integer)id;
	}
	
	//Update and remove process
	public boolean update(String query){
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		int id=0;
		try{
		id=session.createSQLQuery(query).executeUpdate();
		transaction.commit();
		}
		catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
		}
		finally{
			session.close();
		}
		if(id>0)
			return true;
		else 
			return false;
	}
	
	
	public List<?> getQuery(String querystr){
		Session session = null;
		Transaction tx=null;
		List<?> result;
		
		try{
			session= HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			Query query 	= session.createSQLQuery(querystr);
			result	= query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
			tx.commit();
		}
		catch(Exception e){ 
			e.printStackTrace();
			return null;
			
		}finally{
			session.close();
 		}
		return result;
	}
	
}
