package com.animalcare.action;


import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.animalcare.helper.EcomRegHelper;
import com.animalcare.model.EcomCattleInsert;
import com.animalcare.model.EcomProductInsert;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.EcomRegService;
import com.animalcare.util.HibernateUtil;
import com.animalcare.vo.CattleVO;
import com.animalcare.vo.EcomRegVo;
import com.animalcare.vo.ProductVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;



@Path("/ecomregister")
public class EcomRegAction {

	@InjectParam
	EcomRegHelper ecomreghelper;
	
	@InjectParam
	EcomRegService ecomregservice;
	
	
	@Context HttpServletRequest req;
	public static ResourceBundle resourceBundle;
	
	
	@POST
	@Path("/ecomregregister")
	@Consumes(MediaType.APPLICATION_JSON)   
 	@Produces(MediaType.APPLICATION_JSON)
	public Response saveEcomReg(EcomRegVo  ecomregvo){
		try {
		int status= ecomreghelper.saveEcomRegContent(ecomregvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	//update products data
	@POST
	@Path("/productupdate")
	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
	public Response productsUpdate(ProductVO productvo){
		Session session = null;
		Transaction tx=null;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();	
			int result = 0;
			Query query = null;
			EcomProductInsert ecomproductinsert = new EcomProductInsert();
			
			String productname = productvo.getProduct_name();
			String productcategory = productvo.getProduct_category();
			String productprice = productvo.getProduct_price();
			String productimage = productvo.getProduct_image();
            String sellerid = productvo.getSellerid();	
            String productoffer = productvo.getProduct_offer();
			int productid = productvo.getProduct_id();
			
			/*String hql="update UserRegistration set customer_name=:customer_name,customer_phone=:customer_phone,customer_street=:customer_street,userphoto=:userphoto where customer_id=:customer_id";*/
			
			String hql="update EcomProductInsert set product_name=:product_name,product_category=:product_category,product_price=:product_price,product_image=:product_image,sellerid=:sellerid,product_offer=:product_offer where product_id=:product_id";
			
			query = session.createQuery(hql);
			query.setParameter("product_name",productname);
			query.setParameter("product_category",productcategory);
			query.setParameter("product_image",productimage );
			query.setParameter("sellerid", sellerid);
			query.setParameter("product_offer",productoffer);
			query.setParameter("product_price",productprice);
			query.setParameter("product_id",productid );
			
			
			
			System.out.println("products view:"+productid);
			result = query.executeUpdate();
			System.out.println("news update value"+result);
			tx.commit();
			if(result>0)
				return Response.status(Status.OK).entity(Status.ACCEPTED).build();
			else 
				return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	@POST
	@Path("/productdelete/{product_id}")
	public Response deleteProducts(@PathParam("product_id") String product_id){
	
		boolean b = ecomregservice.update("delete from drpro.e_commerce_product where product_id='"+product_id+"'");
		if(b)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
	}
	
	@POST
	@Path("/cattledelete/{cattleid}")
	public Response deleteCattle(@PathParam("cattleid") String cattleid){
	
		boolean b = ecomregservice.update("delete from drpro.cattle_market where cattleid='"+cattleid+"'");
		if(b)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
	}
	
	
	
	
	@POST
	@Path("/cattleupdate")
	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
	public Response cattleUpdate(CattleVO cattlevo){
		Session session = null;
		Transaction tx=null;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();	
			int result = 0;
			Query query = null;
			EcomCattleInsert ecomcattleinsert =new EcomCattleInsert();
			
			String petname = cattlevo.getPetname();
			String petcategory = cattlevo.getPet_category();
			String cattleprice = cattlevo.getCattle_price();
			String cattleimage = cattlevo.getCattle_image();
            String sellerid = cattlevo.getSellerid();	
            int  cattleid =cattlevo.getCattleid();
			
			/*String hql="update EcomProductInsert set product_name=:product_name,product_category=:product_category,product_price=:product_price,product_image=:product_image,sellerid=:sellerid,product_offer=:product_offer where product_id=:product_id";*/
             
            String hql ="update EcomCattleInsert set petname=:petname,pet_category=:pet_category,cattle_price=:cattle_price,cattle_image=:cattle_image,sellerid=:sellerid where cattleid=:cattleid";
			query = session.createQuery(hql);
			query.setParameter("petname",petname);
			query.setParameter("pet_category",petcategory);
			query.setParameter("cattle_price",cattleprice );
			query.setParameter("sellerid", sellerid);
			query.setParameter("cattle_image",cattleimage);
			query.setParameter("cattleid",cattleid);
			
			
			
			System.out.println("cattle view:"+cattleid);
			result = query.executeUpdate();
			System.out.println("news update value"+result);
			tx.commit();
			if(result>0)
				return Response.status(Status.OK).entity(Status.ACCEPTED).build();
			else 
				return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
	
	
	
	@POST
	@Path("/ecomproductsave")
	@Consumes(MediaType.APPLICATION_JSON)   
 	@Produces(MediaType.APPLICATION_JSON)
	public Response saveproduct(ProductVO productvo){
		try {
		int status= ecomreghelper.saveEcomProductInsert(productvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("/ecomcattlesave")
	@Consumes(MediaType.APPLICATION_JSON)   
 	@Produces(MediaType.APPLICATION_JSON)
	public Response saveCattle(CattleVO cattlevo){
		try {
		int status= ecomreghelper.saveEcomCattleInsert(cattlevo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	
	@GET
	@Path("/typeofecomreg/{typeofreg}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gettypeofregistration(@PathParam("typeofreg") String typeofreg) {
		try{
			java.util.List<?> getreg = ecomregservice.getQuery("SELECT * FROM seller where typeofreg='"+typeofreg+"'");
			if(getreg.size()>0){
				return Response.status(Status.OK).entity(getreg).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	@GET
	@Path("/getproducts/{sellerid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProducts(@PathParam("sellerid") String sellerid) {
		try{
			java.util.List<?> getproducts = ecomregservice.getQuery("SELECT * FROM e_commerce_product where sellerid='"+sellerid+"'");
			if(getproducts.size()>0){
				return Response.status(Status.OK).entity(getproducts).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/getcattle/{sellerid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getcattle(@PathParam("sellerid") String sellerid) {
		try{
			java.util.List<?> getproducts = ecomregservice.getQuery("SELECT * FROM cattle_market where sellerid='"+sellerid+"'");
			if(getproducts.size()>0){
				return Response.status(Status.OK).entity(getproducts).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	

	@GET
	@Path("/getecomreglist/{sellerid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response geEComRegList(@PathParam("sellerid") String sellerid) {
		try{
			java.util.List<?> getreg = ecomregservice.getQuery("SELECT * FROM seller where sellerid='"+sellerid+"'");
			if(getreg.size()>0){
				return Response.status(Status.OK).entity(getreg).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
		
	
	@GET
	@Path("/getecomemail/{selleremail}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEcomMailList(@PathParam("selleremail") String selleremail) {
		try{
			java.util.List<?> getreg = ecomregservice.getQuery("SELECT * FROM seller where selleremail='"+selleremail+"'");
			if(getreg.size()>0){
				return Response.status(Status.OK).entity(getreg).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	
	
}
