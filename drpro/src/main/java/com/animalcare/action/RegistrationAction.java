package com.animalcare.action;

import java.awt.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST; 
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


import com.animalcare.helper.EcomRegHelper;
import com.animalcare.helper.RegistrationHelper;
import com.animalcare.model.AnimalRegisration;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.EcomRegService;
import com.animalcare.service.RegistrationService;
import com.animalcare.util.HibernateUtil;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.CustomerProductVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.PetBoundariesVO;
import com.animalcare.vo.UserRegistrationVO;

import com.sun.jersey.api.core.InjectParam;


@Path("/registration")
public class RegistrationAction {

	@InjectParam
	RegistrationHelper registrationhelper;
	
	@InjectParam
	EcomRegHelper ecomreghelper;
	
	@InjectParam
	EcomRegService ecomregservice;
	
	@InjectParam
	RegistrationService registrationservice;
	
	//customer registration
	@POST
	@Path("/userregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveregistration(UserRegistrationVO userregistrationvo){
		int status= registrationhelper.saveregistration(userregistrationvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		
	}
	
	//animal registration
	@POST
	@Path("/petregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savepetregistration(AnimalRegisration animalreg){
		int status= registrationhelper.savepetregistration(animalreg);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
	}
	
	//vet doctor registration
	@POST
	@Path("/vetdocregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savedocregistration(DoctorRegistrationVO doctorregistrationvo){
		int status= registrationhelper.savedocregistration(doctorregistrationvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
	}
	
	
	//For doctor appointment
	@POST
	@Path("/userrequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveappointment(AppointmentVO appointmentvo){
		int status= registrationhelper.saveappointment(appointmentvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		
	}
	
	
	//Pet Boundaries Circle
	@POST
	@Path("/petboundary")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response petboundaries(PetBoundariesVO petboundariesvo){
		int status= registrationhelper.savepetboundary(petboundariesvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		
	}
	
	//Save customer's product
	@POST
	@Path("/saveuserproduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response userproduct(CustomerProductVO customerproductvo){
		int status= registrationhelper.savecustomerproduct(customerproductvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		
	}
	
	
	@GET
	@Path("/typeofreg/{typeofreg}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gettypeofregistration(@PathParam("typeofreg") String typeofreg) {
		try{
			java.util.List<?> getreg = registrationservice.getQuery("SELECT * FROM customer_table where typeofreg='"+typeofreg+"'");
			if(getreg.size()>0){
				return Response.status(Status.OK).entity(getreg).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//update customer data
	@POST
	@Path("/userupdate")
	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
	public Response userupdate(UserRegistrationVO userregistrationvo){
		Session session = null;
		Transaction tx=null;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();	
			int result = 0;
			Query query = null;
			
			UserRegistration userregistration = new UserRegistration();
			
			String customername = userregistrationvo.getCustomer_name();
			String phoneno = userregistrationvo.getCustomer_phone();
			String address = userregistrationvo.getCustomer_street();
			String photo = userregistrationvo.getUserphoto();
			String docapp = userregistrationvo.getDoctorappointment();
			
			int customerid = userregistrationvo.getCustomer_id();
			
			String hql="update UserRegistration set customer_name=:customer_name,customer_phone=:customer_phone,customer_street=:customer_street,userphoto=:userphoto,doctorappointment=:doctorappointment where customer_id=:customer_id";
			
			query = session.createQuery(hql);
			query.setParameter("customer_name", customername);
			query.setParameter("customer_phone", phoneno);
			query.setParameter("customer_street",address );
			query.setParameter("userphoto", photo);
			query.setParameter("doctorappointment", docapp);
			query.setParameter("customer_id", customerid);
			
			System.out.println("customer_id view:"+customerid);
			result = query.executeUpdate();
			System.out.println("customer update value"+result);
			tx.commit();
			if(result>0)
				return Response.status(Status.OK).entity(Status.ACCEPTED).build();
			else 
				return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}	
	
	//for user request to doctor for appointment --status has pending or confirmed 
	@POST
	@Path("/userappointment")
	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
	public Response userappoint(UserRegistrationVO userregistrationvo){
		Session session = null;
		Transaction tx=null;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();	
			int result = 0;
			Query query = null;
		
			String docapp = userregistrationvo.getDoctorappointment();
			int customerid = userregistrationvo.getCustomer_id();
			
			String hql="update UserRegistration set doctorappointment=:doctorappointment where customer_id=:customer_id";
			
			query = session.createQuery(hql);
			
			query.setParameter("doctorappointment", docapp);
			query.setParameter("customer_id", customerid);
			
			System.out.println("customer_id view:"+customerid);
			result = query.executeUpdate();
			System.out.println("appointment update value"+result);
			tx.commit();
			if(result>0)
				return Response.status(Status.OK).entity(Status.ACCEPTED).build();
			else 
				return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	//for doctor dashboard
	@GET
	@Path("/doctypeofreg/{typeofreg}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gettypeofdocregistration(@PathParam("typeofreg") String typeofreg) {
		try{
			java.util.List<?> getregdoc = registrationservice.getQuery("SELECT * FROM vetdoctor_table where typeofreg='"+typeofreg+"'");
			if(getregdoc.size()>0){
				return Response.status(Status.OK).entity(getregdoc).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}

	//Getting all pets for find the duplicate device id
	@GET
	@Path("/getallpetlist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getallpetlist() {
		try{
			java.util.List<?> getallpet = registrationservice.getQuery("SELECT * FROM animal");
			if(getallpet.size()>0){
				return Response.status(Status.OK).entity(getallpet).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/getanimallist/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpetlist(@PathParam("customer_id") String customer_id) {
		try{
			java.util.List<?> getanimal = registrationservice.getQuery("SELECT * FROM animal where customer_id='"+customer_id+"'");
			if(getanimal.size()>0){
				return Response.status(Status.OK).entity(getanimal).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/searchpetlist/{animal_category}/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchpetlist(@PathParam("animal_category") String animal_category,@PathParam("customer_id") String customer_id) {
		try{
			java.util.List<?> getsearchpet = registrationservice.getQuery("SELECT * FROM animal WHERE animal_category LIKE '%"+animal_category+"%' and customer_id='"+customer_id+"'");
			if(getsearchpet.size()>0){
				return Response.status(Status.OK).entity(getsearchpet).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//for getting individual animal details
	@GET
	@Path("/getindividualpet/{animal_category}/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpetindividual(@PathParam("animal_category") String animal_category,@PathParam("customer_id") String customer_id) {
		try{
			java.util.List<?> getoneanimal = registrationservice.getQuery("SELECT * FROM animal where animal_category='"+animal_category+"' and customer_id='"+customer_id+"'");
			if(getoneanimal.size()>0){
				return Response.status(Status.OK).entity(getoneanimal).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//for chart process
	@GET
	@Path("/getanimalchart/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpetchart(@PathParam("customer_id") String customer_id) {    //SELECT animal_category AS x, COUNT(*) AS `value` FROM drpro.animal GROUP BY animal_category--->get total pet
		try{
			java.util.List<?> getanimalchart = registrationservice.getQuery("SELECT customer_id,animal_category AS label,COUNT(*) AS `value` FROM animal where customer_id='"+customer_id+"' GROUP BY animal_category");
			if(getanimalchart.size()>0){
				return Response.status(Status.OK).entity(getanimalchart).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/getindividualuser/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getindividualuser(@PathParam("customer_id") String customer_id) {
		try{
			java.util.List<?> getindividual = registrationservice.getQuery("SELECT * FROM customer_table where customer_id='"+customer_id+"'");
			if(getindividual.size()>0){
				return Response.status(Status.OK).entity(getindividual).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//Gettig all doctor list
	@GET 
	@Path("/getalldoclist/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)  //
	public Response getalldoclist(@PathParam("customer_id") String customer_id) {
		try{
			java.util.List<?> getalldoclist = registrationservice.getQuery("select a.customer_name,b.doctorid,b.doctor_name,b.doctor_email,b.dateandtime,b.doctor_phoneno,b.street,b.typeofreg,b.doctor_status,b.latitude,b.longitude from drpro.customer_table a, drpro.vetdoctor_table b where customer_id='"+customer_id+"'");
			if(getalldoclist.size()>0){
				return Response.status(Status.OK).entity(getalldoclist).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	//for neayby doctor list
	@GET 
	@Path("/getdoclist/{customer_id}/{customer_street}")
	@Produces(MediaType.APPLICATION_JSON)  //select a.customer_name,b.doctorid,b.doctor_name,b.doctor_email,b.dateandtime,b.doctor_phoneno,b.location,b.typeofreg,b.doctor_status,b.latitude,b.longitude from customer_table a, vetdoctor_table b where customer_id='"+customer_id+"'
	public Response getdoclist(@PathParam("customer_id") String customer_id,@PathParam("customer_street") String customer_street) {
		try{
			java.util.List<?> getdoclist = registrationservice.getQuery("select a.customer_name,b.doctorid,b.doctor_name,b.doctor_email,b.dateandtime,b.doctor_phoneno,b.street,b.typeofreg,b.doctor_status,b.latitude,b.longitude from drpro.customer_table a, drpro.vetdoctor_table b where a.customer_street=b.city and customer_street like '%"+customer_street+"%' and customer_id='"+customer_id+"'");
			if(getdoclist.size()>0){
				return Response.status(Status.OK).entity(getdoclist).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	//Count for doctor appointment from customer side and view the count doctor side
	@GET
	@Path("/getappointcount/{doctorid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getappointmentcount(@PathParam("doctorid") String doctorid) {    
		try{
			java.util.List<?> getappointmentcount = registrationservice.getQuery("select doctorid,count(*) as count from appointment_table where doctorid='"+doctorid+"'");
			if(getappointmentcount.size()>0){
				return Response.status(Status.OK).entity(getappointmentcount).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	//doctor view confirmed
	@GET
	@Path("/appointconfirmed/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getappointconfirmation(@PathParam("customer_id") String customer_id) {    
		try{
			java.util.List<?> getappointmentconfirmed = registrationservice.getQuery("select a.doctorid,a.doctor_name,a.doctor_email,b.appointmentid,b.request,b.docappointmentdate,c.animal_category,c.animal_name,c.age,c.address,c.gender,c.uid from vetdoctor_table a,appointment_table b,animal c"
					+ " where b.animalid=c.animalid and a.doctorid=b.doctorid  and b.customer_id=c.customer_id and b.customer_id='"+customer_id+"' order by appointmentid desc");
			    if(getappointmentconfirmed.size()>0){
			    	return Response.status(Status.OK).entity(getappointmentconfirmed).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	}  
	
	/*@GET
	@Path("/getdiseasechart/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdiseasechart(@PathParam("customer_id") String customer_id) {    //select animalid,animal_name,animal_category as label,0+disease_range as y from drpro.animal where customer_id='"+customer_id+"
		
			JSONObject results = new JSONObject();
		   try{
			
			String getdiseasechart = "select animalid,animal_name,animal_category as name,0+disease_range as data from drpro.animal where customer_id='"+customer_id+"'";
			    		    
			    java.util.List<?> getpetQuery = HibernateUtil.gteQueryList(getdiseasechart);
			    
			    //JsonArray results2 = new JsonArray();
			    JSONArray results2 = new JSONArray();
			    if(getpetQuery!=null && getpetQuery.size() !=0){
				    for(int i=0;i<getpetQuery.size();i++){
				    	JSONObject obj1 = new JSONObject();
				    	JSONObject obj2 = new JSONObject();
				    	Map map = (HashMap)getpetQuery.get(i);
				    	System.out.println("Line Chart Data:"+map);
				    	obj1.put("type", "spline");
				    	obj1.put("visible", "false");
				    	
				    	System.out.println("Datapoint"+results2);
				    	JSONArray results3 = new JSONArray();
				    	obj2.put("label", "dfd");
				    	obj2.put("Y", 2.22);                          
				    	results3.put(obj2);
				    	obj1.put("datapoints", results3);
				    	System.out.println("DataPoints value:"+obj1);
				    }
				   
				    results.put("data", results2);
				    
				    System.out.println("Result value:"+results);
				 	return Response.status(Status.OK).entity(results).build();
				}
			   	else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
		   }catch (Exception e) {
			   return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
		}
		
			
	 }*/
	
	//getting all pet details fot line chart
	@GET
	@Path("/getdiseasechart/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdiseasechart(@PathParam("customer_id") String customer_id) {      //select animalid,animal_category as label,0+disease_range as y from drpro.animal where customer_id='"+customer_id+"'
		try{
			java.util.List<?> getdiseasechart = registrationservice.getQuery("select animalid,animal_name,animal_category as name,0+disease_range  as data,disease_name,disease_date from animal where customer_id='"+customer_id+"'");
			    if(getdiseasechart.size()>0){
			    	return Response.status(Status.OK).entity(getdiseasechart).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	 }
	
	//Getting individual pet category and their details
	@GET
	@Path("/getpetcategorychart/{customer_id}/{animal_category}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpetcategorychart(@PathParam("customer_id") String customer_id,@PathParam("animal_category") String animal_category) {      //select animalid,animal_category as label,0+disease_range as y from drpro.animal where customer_id='"+customer_id+"'
		try{
			java.util.List<?> getpetchart = registrationservice.getQuery("select animalid,animal_name,animal_category as name,0+disease_range as data,disease_name,disease_date from animal where customer_id='"+customer_id+"' and animal_category='"+animal_category+"'");
			    if(getpetchart.size()>0){
			    	return Response.status(Status.OK).entity(getpetchart).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	 }
	
	
	//Get pet history
	@GET
	@Path("/getpethistory/{animalid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getpethistory(@PathParam("animalid") String animalid) {    
		try{
			java.util.List<?> getpethistory = registrationservice.getQuery("select a.animalid,a.animal_category,a.animal_name,a.address,a.age,a.disease_range,b.customer_name,b.customer_street,b.customer_email,b.customer_phone"
					+ " from drpro.animal a,drpro.customer_table b where a.customer_id=b.customer_id and a.animalid='"+animalid+"'");
			    if(getpethistory.size()>0){
			    	return Response.status(Status.OK).entity(getpethistory).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	}
	
	    //for product list view
			@GET
			@Path("/getcustomerproduct")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getCustomerProduct() {
				try{
					java.util.List<?> productlist = ecomregservice.getQuery("SELECT * FROM e_commerce_product");
					if(productlist.size()>0){
						return Response.status(Status.OK).entity(productlist).build();
					}
					else{
						return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
					}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
			}
			
			//for cattle list
			@GET
			@Path("/getcustomercattle")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getCustomerCattle() {
				try{
					java.util.List<?> cattlelist = ecomregservice.getQuery("SELECT * FROM cattle_market");
					if(cattlelist.size()>0){
						return Response.status(Status.OK).entity(cattlelist).build();
					}
					else{
						return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
					}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
			}
			
			//Getting animal category list for pie chart click
			@GET
			@Path("/getanimalcategory/{animal_category}/{customer_id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getanimalcategory(@PathParam("animal_category") String animal_category,@PathParam("customer_id") String customer_id) {    
				try{
					java.util.List<?> getanimalcategory = registrationservice.getQuery("select * from animal where animal_category='"+animal_category+"' and customer_id='"+customer_id+"'");
					    if(getanimalcategory.size()>0){
					    	return Response.status(Status.OK).entity(getanimalcategory).build();
						}
						else{
							return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
						}
					}catch(Exception e){
						e.printStackTrace();
						return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
					}
			}  
			
			
			//Getting the product list
			@GET
			@Path("/productlist/{customer_id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getproductlist(@PathParam("customer_id") String customer_id) {
				try{
					java.util.List<?> getreg = registrationservice.getQuery("SELECT * FROM customer_product where customer_id='"+customer_id+"'");
					if(getreg.size()>0){
						return Response.status(Status.OK).entity(getreg).build();
					}
					else{
						return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
					}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
			}
			
			
			//Getting the circle list
			@GET
			@Path("/getcirclelist/{customer_id}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getcirclelist(@PathParam("customer_id") String customer_id) {
				try{
					java.util.List<?> getreg = registrationservice.getQuery("select * from pet_boundaries where customer_id='"+customer_id+"'");
					if(getreg.size()>0){
						return Response.status(Status.OK).entity(getreg).build();
					}
					else{
						return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
					}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
			}
			
			@POST
			@Path("/removecircle/{pet_boundariesid}")
			public Response removecircle(@PathParam("pet_boundariesid") String pet_boundariesid) {
			
				boolean b = ecomregservice.update("delete from pet_boundaries where pet_boundariesid='"+pet_boundariesid+"'");
				if(b)
					return Response.status(Status.OK).entity(Status.ACCEPTED).build();
				else
					return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
			}
			
			
	
}
